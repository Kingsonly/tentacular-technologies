  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="<?php echo URL?>dashboard"><i class="fa fa-dashboard"></i>Dashboard</a></li>      </ol>
    </section>
      <div class="container">
       <section class="content">
       <div class="row">
               <div class="col-md-6">
          <div class="box box-solid">
            <div class="box-header with-border">

              <h3 class="box-title">Admin Info</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <dl class="dl-horizontal">
                <dt>Admin id:</dt>
                <dd><?php echo $_SESSION['admin_id']; ?></dd>
                <dt>Fullname:</dt>
                <dd><?php echo $_SESSION['fullname']; ?></dd>
                <dt>Details:</dt>
                <dd><?php echo $_SESSION['details']; ?></dd>
                <dt>E-mail:</dt>
                <dd><?php echo $_SESSION['email']; ?></dd>
                <dt>Phone:</dt>
                <dd><?php echo $_SESSION['phone']; ?></dd>
              </dl>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->
       </div>
       <?php if (isset($_SESSION['message_1'])) {
        $message_1 = $_SESSION['message_1'];
        if ($message_1 == "Multiple records are not allowed") { 
           echo "<div class='row'>
        <div class='col-md-6'>
          <div class='box box-default'>
       <div class='box-body'><div class='alert alert-danger alert-dismissible'>\n
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>\n
                <h4><i class='icon fa fa-ban'></i> Alert!</h4>\n
                $message_1!\n</div>
         </div>
</div>
</div>
</div>";
        }
          if($message_1 == "Query failed") {
          echo "
              <div class='row'>
        <div class='col-md-6'>
          <div class='box box-default'>
       <div class='box-body'><div class='alert alert-danger alert-dismissible'>\n
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>\n
                <h4><i class='icon fa fa-ban'></i> Alert!</h4>\n
                $message_1!\n</div>
         </div>
</div>
</div>
</div>";
              }if ($message_1 == 'Record Successfully Added'){
                echo "
              <div class='row'>
        <div class='col-md-6'>
          <div class='box box-default'>
       <div class='box-body'><div class='alert alert-info alert-dismissible'>\n
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>\n
                <h4><i class='icon fa fa-info'></i> Alert!</h4>\n
                $message_1!\n
              </div>
         </div>
</div>
</div>
</div>";
              }
               if ($message_1 == 'Record Successfully Updated'){
              echo "
              <div class='row'>
        <div class='col-md-6'>
          <div class='box box-default'>
       <div class='box-body'><div class='alert alert-info alert-dismissible'>\n
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>\n
                <h4><i class='icon fa fa-info'></i> Alert!</h4>\n
                $message_1!\n
              </div>
         </div>
</div>
</div>
</div>";
               }
               if ($message_1 == 'Record Successfully Deleted'){
                echo "
              <div class='row'>
        <div class='col-md-6'>
          <div class='box box-default'>
       <div class='box-body'><div class='alert alert-info alert-dismissible'>\n
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>\n
                <h4><i class='icon fa fa-info'></i> Alert!</h4>\n
                $message_1!\n
              </div>
         </div>
</div>
</div>
</div>";
              }
         } ?>
</section>
      </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->