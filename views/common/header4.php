<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Tentacular TGS | Admin</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="public/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="public/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="public/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="public/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="public/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="public/css/style_2.css">
  <link rel="stylesheet" href="public/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="skin-blue fixed" data-spy="scroll" data-target="#scrollspy">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <!-- Logo -->
        <a href="<?php echo URL?>records" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>T</b>TGS</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Tentacular</b>TGS</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li><a href="<?php echo URL?>admin_dashboard">Back to Dashboard</a></li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <div class="sidebar" id="scrollspy">

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="nav sidebar-menu">
            <li class="header">RECORDS</li>
             <li class="treeview" id="scrollspy-components">
              <a href="javascript:void(0)"><i class="fa fa-circle-o"></i>Admin</a>
              <ul class="nav treeview-menu">
                <li><a href="#new-admin">Register</a></li>
                <li><a href="#update-admin">Update</a></li>
                <li><a href="#delete-admin">Delete</a></li>
              </ul>
            </li>
            <li class="treeview" id="scrollspy-components">
              <a href="javascript:void(0)"><i class="fa fa-circle-o"></i>Students</a>
              <ul class="nav treeview-menu">
                <li><a href="#new-student">Add New</a></li>
                <li><a href="#update-student">Update</a></li>
                <li><a href="#delete-student">Delete</a></li>
              </ul>
            </li>
             <li class="treeview" id="scrollspy-components">
              <a href="javascript:void(0)"><i class="fa fa-circle-o"></i>Clients</a>
              <ul class="nav treeview-menu">
                <li><a href="#new-client">Register</a></li>
                <li><a href="#update-client">Update</a></li>
                <li><a href="#delete-client">Delete</a></li>
              </ul>
            </li> 
            <li class="treeview" id="scrollspy-components">
              <a href="javascript:void(0)"><i class="fa fa-circle-o"></i>Partners</a>
              <ul class="nav treeview-menu">
                <li><a href="#new-partner">Register</a></li>
                <li><a href="#delete-partner">Delete</a></li>
              </ul>
            </li> 
             <li class="treeview" id="scrollspy-components">
              <a href="javascript:void(0)"><i class="fa fa-circle-o"></i>Testimonials</a>
              <ul class="nav treeview-menu">
                <li><a href="#new-testimonial">Add New</a></li>
                <li><a href="#delete-testimonial">Delete</a></li>
              </ul>
            </li>
            <li class="treeview" id="scrollspy-components">
              <a href="javascript:void(0)"><i class="fa fa-circle-o"></i>Services</a>
              <ul class="nav treeview-menu">
                <li><a href="#new-service">Add New</a></li>
                <li><a href="#delete-service">Delete</a></li>
              </ul>
            </li>
            <li class="treeview" id="scrollspy-components">
              <a href="javascript:void(0)"><i class="fa fa-circle-o"></i>Terms</a>
              <ul class="nav treeview-menu">
                <li><a href="#new-term">Add New</a></li>
                <li><a href="#delete-term">Delete</a></li>
              </ul>
            </li>
          </ul>
        </div>
        <!-- /.sidebar -->
      </aside>