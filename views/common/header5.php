<?php 
if(!isset($_GET['url'])){
    $_GET['url']='student_dashboard';
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Tentacular TGS | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="shortcut icon" href="public/images/favicon.gif">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="public/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="public/fonts/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="public/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="public/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="public/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="public/dist/css/skins/skin-red.min.css">
  <link rel="stylesheet" href="public/dist/css/skins/skin-red.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="public/plugins/iCheck/flat/red.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="public/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="public/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="public/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="public/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<!-- Bootstrap CSS  -->
  <link rel="stylesheet" href="public/css/bootstrap.min.css" type="text/css" media="screen">

  <!-- Margo JS  -->
  <script type="text/javascript" src="public/js/jquery-2.1.4.min.js"></script>
  <script type="text/javascript" src="public/js/jquery.migrate.js"></script>
  <script type="text/javascript" src="public/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="public/js/jquery.nicescroll.min.js"></script>
  <script type="text/javascript" src="public/js/jquery.slicknav.js"></script>
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo URL?>student_dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>T</b>TGS</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Tentacular</b>TGS</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Notifications: style can be found in dropdown.less -->
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="public/images/students/<?php echo $pix;?>" class="user-image" alt="User Image">
              <span class="hidden-xs">Hello <?php echo "$username"; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <p>
                  $username
                  <small>$details</small>
                </p>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
<a href="<?php echo URL?>views/student_logout.php">Sign out</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="<?php if($_GET['url']=='student_dashboard'){echo'active';} ?>">
          <a href="<?php echo URL?>student_dashboard">
      <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li><li class="<?php if($_GET['url']=='classmates'){echo'active';} ?>">
          <a href="<?php echo URL?>classmates">
            <i class="fa fa-bullseye"></i> <span>Classmates </span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red"><?php echo $classmates ?></small>
            
          </a>
          </li>
          <li class="<?php if($_GET['url']=='materials'){echo'active';} ?>">
          <a href="<?php echo URL?>materials">
            <i class="fa fa-bullseye"></i>
            <span>Materials</span>
          </a>
          </li>
        <li class="<?php if($_GET['url']=='student_mailbox'){echo'active';} ?>">
          <a href="<?php echo URL?>student_mailbox">
            <i class="fa fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red"><?php echo "$unread";?></small>
            </span>
          </a>
        </li>
        <li class="<?php if($_GET['url']=='weekly_challenge'){echo'active';} ?>">
          <a href="<?php echo URL?>weekly_challenge">
            <i class="fa fa-envelope"></i> <span>Weekly Challenge</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red"></small>
            </span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>