<?php 
if(!isset($_GET['url'])){
    $_GET['url']='index';
}
?>
<body>

  <!-- Full Body Container -->
  <div id="container">


    <!-- Start Header Section -->
    <div class="hidden-header"></div>
    <header class="clearfix">

      <!-- Start Top Bar -->
      <div class="top-bar">
        <div class="container">
          <div class="row">
            <div class="col-md-7">
              <!-- Start Contact Info -->
              <ul class="contact-details">
                <li><a href="#"><i class="fa fa-map-marker"></i> House-54/A, Mabushi, FCT Abuja</a>
                </li>
                <li><a href="#"><i class="fa fa-envelope-o"></i> info@tentacularltd.com</a>
                </li>
                <li><a href="#"><i class="fa fa-phone"></i> +2348153259099</a>
                </li>
              </ul>
              <!-- End Contact Info -->
            </div>
            <!-- .col-md-6 -->
            <div class="col-md-5">
              <!-- Start Social Links -->
              <ul class="social-list">
                <li>
                  <a class="facebook itl-tooltip" data-placement="bottom" title="Facebook" href="#"><i class="fa fa-facebook"></i></a>
                </li>
                <li>
                  <a class="twitter itl-tooltip" data-placement="bottom" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                </li>
                <li>
                  <a class="google itl-tooltip" data-placement="bottom" title="Google Plus" href="#"><i class="fa fa-google-plus"></i></a>
                </li>
                <li>
                  <a class="dribbble itl-tooltip" data-placement="bottom" title="Dribble" href="#"><i class="fa fa-dribbble"></i></a>
                </li>
                <li>
                  <a class="linkdin itl-tooltip" data-placement="bottom" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                </li>                <li>
                  <a class="instgram itl-tooltip" data-placement="bottom" title="Instagram" href="#"><i class="fa fa-instagram"></i></a>
                </li>
              </ul>
              <!-- End Social Links -->
            </div>
            <!-- .col-md-6 -->
          </div>
          <!-- .row -->
        </div>
        <!-- .container -->
      </div>
      <!-- .top-bar -->
      <!-- End Top Bar -->


      <!-- Start  Logo & Naviagtion  -->
      <div class="navbar navbar-default navbar-top">
        <div class="container">
          <div class="navbar-header">
            <!-- Stat Toggle Nav Link For Mobiles -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
            <!-- End Toggle Nav Link For Mobiles -->
            <a class="navbar-brand" href="<?php echo URL?>index">
              <img alt="" src="public/images/tent.png" style="height: 40px; width: 40px;">
            </a>
          </div>
          <div class="navbar-collapse collapse">
            <!-- Stat Search -->
            <div class="search-side">
              <a class="show-search"><i class="fa fa-search"></i></a>
              <div class="search-form">
                <form autocomplete="off" role="search" method="get" class="searchform" action="#">
                  <input type="text" value="" name="s" id="s" placeholder="Search the site...">
                </form>
              </div>
            </div>
            <!-- End Search -->
            <!-- Start Navigation List -->
            <ul class="nav navbar-nav navbar-right">
              <li>
                <a class="<?php if($_GET['url']=='index'){echo'active';} ?>" href="<?php echo URL?>index">Home</a>
                
              </li>
              <li>
                <a class="<?php if($_GET['url']=='about'){echo'active';} ?>" href="<?php echo URL?>about">About Us</a>
                <ul class="dropdown">
                 <!-- <li><a href="<?php echo URL?>about">About</a>
                  </li>
                    
                  <li><a href="services.html">Services</a>
                  </li>
                  <li><a href="right-sidebar.html">Right Sidebar</a>
                  </li>
                  <li><a href="left-sidebar.html">Left Sidebar</a>
                  </li>
                  <li><a href="404.html">404 Page</a>
                  </li> -->
                </ul>
              </li>
              <li>
                <a class="<?php if($_GET['url']=='academy' || $_GET['url']=='academy_projects'){echo'active';} ?>" href="<?php echo URL?>academy">Academy</a>
                <ul class="dropdown">
                 <!-- <li><a href="tabs.html">Tabs</a>
                  </li>
                  <li><a href="buttons.html">Buttons</a>
                  </li>
                  <li><a href="action-box.html">Action Box</a>
                  </li>
                  <li><a href="testimonials.html">Testimonials</a>
                  </li>
                  <li><a href="latest-posts.html">Latest Posts</a>
                  </li>
                  <li><a href="latest-projects.html">Latest Projects</a>
                  </li>
                  <li><a href="pricing.html">Pricing Tables</a>
                  </li>
                  <li><a href="animated-graphs.html">Animated Graphs</a>
                  </li>
                  <li><a href="accordion-toggles.html">Accordion & Toggles</a>
                  </li>-->
                </ul>
              </li>
              <li>
                <a class="<?php if($_GET['url']=='portfolio' || $_GET['url']=='single'){echo'active';} ?>" href="<?php echo URL?>portfolio">Portfolio</a>
                
              </li>
             
              <li><a class="<?php if($_GET['url']=='contact'){echo'active';} ?>" href="<?php echo URL?>contact">Contact</a>
              </li>
              <li><a class="<?php if($_GET['url']=='login'){echo'active';} ?>" href="<?php echo URL?>student_login">LOGIN</a>
              </li>
            </ul>
            <!-- End Navigation List -->
          </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class="wpb-mobile-menu">
          <li>
            <a class="active" href="<?php echo URL?>index">Home</a>
            <!-- <ul class="dropdown">
              <li><a class="active" href="<?php echo URL?>index">Home Main Version</a>
              </li>
              <li><a href="index-01.html">Home Version 1</a>
              </li>
              <li><a href="index-02.html">Home Version 2</a>
              </li>
              <li><a href="index-03.html">Home Version 3</a>
              </li>
              <li><a href="index-04.html">Home Version 4</a>
              </li>
              <li><a href="index-05.html">Home Version 5</a>
              </li>
              <li><a href="index-06.html">Home Version 6</a>
              </li>
              <li><a href="index-07.html">Home Version 7</a>
              </li>
            </ul>-->
          </li>
          <li>
            <a href="<?php echo URL?>about">About Us</a>
            <!-- <ul class="dropdown">
              <li><a href="<?php echo URL?>about">About</a>
              </li>
              <li><a href="services.html">Services</a>
              </li>
              <li><a href="right-sidebar.html">FAQ</a>
              </li>
              
              <li><a href="404.html">404 Page</a>
              </li>
            </ul> -->
          </li>
          <li>
                <a href="<?php echo URL?>academy">Academy</a>
           <!--  <ul class="dropdown">
              <li><a href="tabs.html">Tabs</a>
              </li>
              <li><a href="buttons.html">Buttons</a>
              </li>
              <li><a href="action-box.html">Action Box</a>
              </li>
              <li><a href="testimonials.html">Testimonials</a>
              </li>
              <li><a href="latest-posts.html">Latest Posts</a>
              </li>
              <li><a href="latest-projects.html">Latest Projects</a>
              </li>
              <li><a href="pricing.html">Pricing Tables</a>
              </li>
              <li><a href="animated-graphs.html">Animated Graphs</a>
              </li>
              <li><a href="accordion-toggles.html">Accordion & Toggles</a>
              </li>
            </ul> -->
          </li>
          <li>
            <a href="<?php echo URL?>portfolio">Portfolio</a>
           <!--  <ul class="dropdown">
              <li><a href="portfolio-2.html">2 Columns</a>
              </li>
              <li><a href="portfolio-3.html">3 Columns</a>
              </li>
              <li><a href="portfolio-4.html">4 Columns</a>
              </li>
              <li><a href="single-project.html">Single Project</a>
              </li>
            </ul> -->
          </li>
          <li>
            <a href="blog.html">Blog</a>
           <!--  <ul class="dropdown">
              <li><a href="blog.html">Blog - right Sidebar</a>
              </li>
              <li><a href="blog-left-sidebar.html">Blog - Left Sidebar</a>
              </li>
              <li><a href="single-post.html">Blog Single Post</a>
              </li>
            </ul> -->
          </li>
          <li>
            <a href="<?php echo URL?>contact">Contact</a>
          </li>
              <li><a href="<?php echo URL?>student_login">LOGIN</a>
              </li>
        </ul>
        <!-- Mobile Menu End -->

      </div>
      <!-- End Header Logo & Naviagtion -->

    </header>
    <!-- End Header Section -->
