  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gallery
        <small>Upload Images</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="<?php echo URL?>admin_dashboard"><i class="fa fa-dashboard"></i>Dashboard</a></li>      </ol>
    </section>
      <div class="container">
       <section class="content">
       <div class="row">
               <div class="col-md-6">
          <div class="box box-solid">
            <div class="box-header with-border">

              <h3 class="box-title">Admin</h3>
            </div>
            <!-- /.box-header -->
              <form class="form-horizontal" role="form" enctype="multipart/form-data" action="gallery/add_admin_picture" method="POST">
            <div class="box-body">
                <div class="form-group"><label class="col-sm-2 control-label">Name of Admin: </label>
                <div class="col-sm-10"><select  name='username'>
         <?php foreach ($ad as $key => $value) { 
           echo"<option value=$value[username]>$value[username]</option>";
            } ?>
         </select>
         </div>
         </div>
               <div class="form-group"><label class="col-sm-2 control-label">Picture: </label>
                <div class="col-sm-10">
         <input type="hidden" name="MAX_FILE_SIZE" value="500000"/><input type="file" name="pix" size="60" />
         </div>
         </div>
         </div>
         <div class="box-footer"><input type="submit" name="Upload" value="Upload Picture" />
            </div>
</form>
</div>
          <div class="box box-solid">
          <div class="box-header with-border">

              <h3 class="box-title">Clients</h3>
            </div>
 <form class="form-horizontal" role="form" enctype="multipart/form-data" action="gallery/add_client_picture" method="POST">
            <div class="box-body">
                <div class="form-group"><label class="col-sm-2 control-label">Name of Client: </label>
                <div class="col-sm-10"><select  name='name'>
         <?php foreach ($cli as $key => $value) { 
           echo"<option value=$value[name]>$value[name]</option>";
            } ?>
         </select>
         </div>
         </div>
               <div class="form-group"><label class="col-sm-2 control-label">Picture: </label>
                <div class="col-sm-10">
         <input type="hidden" name="MAX_FILE_SIZE" value="500000"/><input type="file" name="pix" size="60" />
         </div>
         </div>
         </div>
         <div class="box-footer"><input type="submit" name="Upload" value="Upload Picture" />
            </div>
</form>
</div>
          <div class="box box-solid">
          <div class="box-header with-border">

              <h3 class="box-title">Partners</h3>
            </div>
 <form class="form-horizontal" role="form" enctype="multipart/form-data" action="gallery/add_partner_picture" method="POST">
            <div class="box-body">
                <div class="form-group"><label class="col-sm-2 control-label">Name of Partner: </label>
                <div class="col-sm-10"><select  name='name'>
         <?php foreach ($par as $key => $value) { 
           echo"<option value=$value[name]>$value[name]</option>";
            } ?>
         </select>
         </div>
         </div>
               <div class="form-group"><label class="col-sm-2 control-label">Picture: </label>
                <div class="col-sm-10">
         <input type="hidden" name="MAX_FILE_SIZE" value="500000"/><input type="file" name="pix" size="60" />
         </div>
         </div>
         </div>
         <div class="box-footer"><input type="submit" name="Upload" value="Upload Picture" />
            </div>
</form>
</div>
          <div class="box box-solid">
          <div class="box-header with-border">

              <h3 class="box-title">Testimonials</h3>
            </div>
 <form class="form-horizontal" role="form" enctype="multipart/form-data" action="gallery/add_testimonial_picture" method="POST">
            <div class="box-body">
                <div class="form-group"><label class="col-sm-2 control-label">Name of Testifier: </label>
                <div class="col-sm-10"><select  name='name'>
         <?php foreach ($test as $key => $value) { 
           echo"<option value=$value[name]>$value[name]</option>";
            } ?>
         </select>
         </div>
         </div>
               <div class="form-group"><label class="col-sm-2 control-label">Picture: </label>
                <div class="col-sm-10">
         <input type="hidden" name="MAX_FILE_SIZE" value="500000"/><input type="file" name="pix" size="60" />
         </div>
         </div>
         </div>
         <div class="box-footer"><input type="submit" name="Upload" value="Upload Picture" />
            </div>
</form>
</div>
          <div class="box box-solid">
          <div class="box-header with-border">

              <h3 class="box-title">Students</h3>
            </div>
 <form class="form-horizontal" role="form" enctype="multipart/form-data" action="gallery/add_student_picture" method="POST">
            <div class="box-body">
                <div class="form-group"><label class="col-sm-2 control-label">Name of Student: </label>
                <div class="col-sm-10"><select  name='name'>
         <?php foreach ($stu as $key => $value) { 
           echo"<option value=$value[username]>$value[username]</option>";
            } ?>
         </select>
         </div>
         </div>
               <div class="form-group"><label class="col-sm-2 control-label">Picture: </label>
                <div class="col-sm-10">
         <input type="hidden" name="MAX_FILE_SIZE" value="500000"/><input type="file" name="pix" size="60" />
         </div>
         </div>
         </div>
         <div class="box-footer"><input type="submit" name="Upload" value="Upload Picture" />
            </div>
</form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->
       </div>
</section>
      </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->