<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

  <!-- Basic -->
  <title>Tentacular Technologies</title>

  <!-- Define Charset -->
  <meta charset="utf-8">

  <!-- Responsive Metatag -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- Page Description and Author -->
  <meta name="description" content="Tentacular Solutions">
  <meta name="author" content="iThemesLab">

  <link rel="shortcut icon" href="public/images/favicon.gif">
  <!-- Bootstrap CSS  -->
  <link rel="stylesheet" href="public/css/bootstrap.min.css" type="text/css" media="screen">

  <!-- Font Awesome CSS -->
  <link rel="stylesheet" href="public/css/font-awesome.min.css" type="text/css" media="screen">

  <!-- Slicknav -->
  <link rel="stylesheet" type="text/css" href="public/css/slicknav.css" media="screen">

  <!-- Margo CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="public/css/style.css" media="screen">

  <!-- Responsive CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="public/css/responsive.css" media="screen">

  <!-- Css3 Transitions Styles  -->
  <link rel="stylesheet" type="text/css" href="public/css/animate.css" media="screen">

  <!-- Color CSS Styles  -->
  <link rel="stylesheet" type="text/css" href="public/css/colors/red.css" title="red" media="screen" />

  <!-- Margo JS  -->
  <script type="text/javascript" src="public/js/jquery-2.1.4.min.js"></script>
  <script type="text/javascript" src="public/js/jquery.migrate.js"></script>
  <script type="text/javascript" src="public/js/modernizrr.js"></script>
  <script type="text/javascript" src="public/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="public/js/jquery.fitvids.js"></script>
  <script type="text/javascript" src="public/js/owl.carousel.min.js"></script>
  <script type="text/javascript" src="public/js/nivo-lightbox.min.js"></script>
  <script type="text/javascript" src="public/js/jquery.isotope.min.js"></script>
  <script type="text/javascript" src="public/js/jquery.appear.js"></script>
  <script type="text/javascript" src="public/js/count-to.js"></script>
  <script type="text/javascript" src="public/js/jquery.textillate.js"></script>
  <script type="text/javascript" src="public/js/jquery.lettering.js"></script>
  <script type="text/javascript" src="public/js/jquery.easypiechart.min.js"></script>
  <script type="text/javascript" src="public/js/jquery.nicescroll.min.js"></script>
  <script type="text/javascript" src="public/js/jquery.parallax.js"></script>
  <script type="text/javascript" src="public/js/mediaelement-and-player.js"></script>
  <script type="text/javascript" src="public/js/jquery.slicknav.js"></script>
  

  <!--[if IE 8]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

</head>