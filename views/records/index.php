      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
          <ol class="breadcrumb">
        <li><a href="<?php echo URL?>admin_dashboard"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">Records</li>
          </ol>
        </div>

        <!-- Main content -->
        <div class="content body">

<section id="new-admin">
  <h2 class="page-header"><a href="#new-admin">Register New Admin</a></h2>
  <form enctype="multipart/form-data" method="post" action="records/add_new_admin" class="form-horizontal" role="form">
              <div class="box-body" style="background-color: #fff;">
                <div class="form-group">
         <label for="username" class="col-sm-2 control-label">Username: </label>
         <div class="col-sm-10"><input id="username" type="text" name="username"></div>
         </div>
                <div class="form-group">
         <label for="password" class="col-sm-2 control-label">Password: </label>
         <div class="col-sm-10"><input id="password" type="password" name="password"></div>
         </div>
                <div class="form-group">
         <label for="fullname" class="col-sm-2 control-label">Full Name: </label>
         <div class="col-sm-10"><input id="fullname" type="text" name="fullname"></div>
         </div>
                <div class="form-group">
         <label for="details" class="col-sm-2 control-label">Details: </label>
         <div class="col-sm-10"><input id="details" type="text" name="details"></div>
         </div>
                <div class="form-group">
         <label for="email" class="col-sm-2 control-label">Email: </label>
         <div class="col-sm-10"><input id="email" type="text" name="email"><br></div>
         </div>
                <div class="form-group">
         <label for="phone" class="col-sm-2 control-label">Phone: </label>
         <div class="col-sm-10"><input id="phone" type="text" name="phone"><br></div>
         </div>
         </div>
         <div class="box-footer">
          <input type="submit" class="btn btn-primary" name="SUBMIT" value="SUBMIT"> 
          </div>
       </form>
</section><br>


<!-- ============================================================= -->

<section id="update-admin">
  <h2 class="page-header"><a href="#update-admin">Update Existing Admin</a></h2>
  <form enctype="multipart/form-data" method="post" action="records/update_existing_admin" class="form-horizontal" role="form">
   <div class="box-body" style="background-color: #fff;">
                <div class="form-group"><label class="col-sm-2 control-label" for="username">Name of Admin: </label>
                <div class="col-sm-10"><select name='username'>
         <?php foreach ($ad as $key => $value) { 
           echo"<option value=$value[username]>$value[username]</option>";
            } ?>
         </select>
         </div>
         </div>
                <div class="form-group"><label for="update_field" class="col-sm-2 control-label">Update field:  </label>
                <div class="col-sm-10"><select name="update_field">
           <option value="username">Username</option>
           <option value="password">Password</option>
           <option value="fullname">Full Name</option>
           <option value="details">Details</option>
           <option value="email">Email</option>
           <option value="phone">Phone</option>
         </select>
         </div>
         </div>
                <div class="form-group"><label class="col-sm-2 control-label" for="update">Update:  </label>
                <div class="col-sm-10"><input type="text" name="update">
                </div>
                </div>
                </div>
                <div class="box-footer">
          <input type="submit" class="btn btn-primary" name="SUBMIT" value="SUBMIT">
          </div>
       </form>
</section>
<br>

<!-- ============================================================= -->

<section id="delete-admin">
  <h2 class="page-header"><a href="#delete-admin">Delete Admin Account</a></h2>
 <form method="post" action="records/delete_existing_admin" class="form-horizontal" role="form">
   <div class="box-body" style="background-color: #fff;">
   <div class="form-group"><label class="col-sm-2 control-label" for="username">Delete Admin:  </label>
   <div class="col-sm-4"><select name='username' class="form-control">
         <?php foreach ($ad as $key => $value) { 
           echo"<option value='$value[username]'>$value[username]</option>";
            } ?>
         </select>
         </div>
         </div>
         </div>
         <div class="box-footer">
         <input type="submit" name="submit" class="btn btn-primary" value="SUBMIT">
         </div>
       </form>
</section><br>


<!-- ============================================================= -->
<section id="new-student">
  <h2 class="page-header"><a href="#new-student">Register New Student</a></h2>
 <form enctype="multipart/form-data" method="post" action="records/add_new_student" class="form-horizontal" role="form">
   <div class="box-body" style="background-color: #fff;">
         <div class="form-group"><label class="col-sm-2 control-label">Username: </label><div class="col-sm-10"><input class="form-control" type="text" name="username">
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Password: </label><div class="col-sm-10"><input class="form-control" type="password" name="password">
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Full Name: </label><div class="col-sm-10"><input class="form-control" type="text" name="fullname">
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Plan: </label><div class="col-sm-10"><select class="form-control" name="plan">
           <option value="basic">Basic</option>
           <option value="advanced">Advanced</option>
           <option value="proffessional">Proffessional</option>
         </select>
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Address: </label><div class="col-sm-10"><input class="form-control" type="text" name="address">
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Email: </label><div class="col-sm-10"><input class="form-control" type="text" name="email">
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Phone: </label><div class="col-sm-10"><input class="form-control" type="text" name="phone">
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Paid: NGN </label><div class="col-sm-10"><input class="form-control" type="number" name="paid">
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Paid Status: </label><div class="col-sm-10"><select class="form-control" name="paid_status">
           <option value="Full-pay">Half-pay</option>
           <option value="Half-pay">Full-pay</option>
         </select>
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Academy Remark: </label><div class="col-sm-10"><input class="form-control" type="text" name="academy_remark">
         </div>
         </div>
         <div class="box-footer">
          <input type="submit" class="btn btn-primary" name="SUBMIT" value="SUBMIT">
          </div>
       </form>
</section><br>


<!-- ============================================================= -->

<section id="update-student">
  <h2 class="page-header"><a href="#update-student">Update Student</a></h2>
  <form enctype="multipart/form-data" method="post" action="records/update_existing_student" class="form-horizontal" role="form">
   <div class="box-body" style="background-color: #fff;">
         <div class="form-group"><label class="col-sm-2 control-label">Name of Student: </label>
         <div class="col-sm-10"><select name='username'>
         <?php foreach ($stu as $key => $value) { 
           echo"<option value=$value[username]>$value[username]</option>";
            } ?>
         </select>
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Update field:  </label>
         <div class="col-sm-10"><select name="update_field">
           <option value="username">Username</option>
           <option value="password">Password</option>
           <option value="fullname">Full Name</option>
           <option value="plan">Plan</option>
           <option value="address">Address</option>
               <option value="email">Email</option>
           <option value="phone">Phone</option>
           <option value="paid_status">Paid Status</option>
           <option value="paid">Paid</option>
           <option value="completed_course">Completed Course</option>
           <option value="academy_remark">Academy Remark</option>
         </select>
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Update:  </label>
         <div class="col-sm-10"><input type="text" name="update">
         </div>
         </div>
         </div>
         <div class="box-footer">
          <input type="submit" class="btn btn-primary" name="SUBMIT" value="SUBMIT">
          </div>
       </form>
</section>
<br>

<!-- ============================================================= -->

<section id="delete-student">
  <h2 class="page-header"><a href="#delete-student">Delete Student Record</a></h2>
  <form method="post" action="records/delete_existing_student" class="form-horizontal" role="form">
   <div class="box-body" style="background-color: #fff;">
         <div class="form-group"><label class="col-sm-2 control-label">Delete Student:  </label>
         <div class="col-sm-4"><select name='username'>
         <?php foreach ($stu as $key => $value) { 
           echo"<option value='$value[username]'>$value[username]</option>";
            } ?>
         </select>
         </div>
         </div>
         </div>
         <div class="box-footer">
         <input type="submit" class="btn btn-primary" name="submit" value="SUBMIT">
         </div>
       </form>
</section>
<br>

<!-- ============================================================= -->

<section id="new-client">
  <h2 class="page-header"><a href="#new-client">Add New Client</a></h2>
  <form enctype="multipart/form-data" method="post" action="records/add_new_client" class="form-horizontal" role="form">
   <div class="box-body" style="background-color: #fff;">
         <div class="form-group"><label class="col-sm-2 control-label">Name: </label>
         <div class="col-sm-10"><input type="text" name="name">
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Details: </label>
         <div class="col-sm-10"><input type="text" name="details">
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Phone: </label>
         <div class="col-sm-10"><input type="text" name="contact">
         </div>
         </div>
         </div>
         <div class="box-footer">
          <input type="submit" class="btn btn-primary" name="SUBMIT" value="SUBMIT">
          </div>
       </form>
</section><br>
<!-- ============================================================= -->

<section id="update-client">
  <h2 class="page-header"><a href="#update-client">Update Client Record</a></h2>
 <form enctype="multipart/form-data" method="post" action="records/update_existing_client" class="form-horizontal" role="form">
   <div class="box-body" style="background-color: #fff;">
              <div class="form-group"><label class="col-sm-2 control-label">Name of Client: </label>
              <div class="col-sm-10"><select name='name'>
         <?php foreach ($cli as $key => $value) { 
           echo"<option value='$value[name]'>$value[name]</option>";
            } ?>
         </select>
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Update field:  </label>
         <div class="col-sm-10"><select name="update_field">
           <option value="name">Name</option>
           <option value="details">Details</option>
           <option value="contact">Contact</option>]
         </select>
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Update:  </label>
         <div class="col-sm-10"><input type="text" name="update">
         </div>
         </div>
         </div>
         <div class="box-footer">
          <input type="submit" class="btn btn-primary" name="SUBMIT" value="SUBMIT">
          </div>
       </form>
</section><br>


<!-- ============================================================= -->

<section id="delete-client">
  <h2 class="page-header"><a href="#delete-client">Delete Client Record</a></h2>
 <form method="post" action="records/delete_existing_client" class="form-horizontal" role="form">
   <div class="box-body" style="background-color: #fff;">
         <div class="form-group"><label class="col-sm-2 control-label">Delete Client:  </label>
         <div class="col-sm-4"><select name='name'>
         <?php foreach ($cli as $key => $value) { 
           echo"<option value='$value[name]'>$value[name]</option>";
            } ?>
         </select>
         </div>
         </div>
         </div>
         <div class="box-footer">
         <input type="submit" class="btn btn-primary" name="submit" value="SUBMIT">
         </div>
       </form>
</section>
<br>

<!-- ============================================================= -->
<section id="new-partner">
  <h2 class="page-header"><a href="#new-partner">Add New Partner</a></h2>
  <form enctype="multipart/form-data" method="post" action="records/add_new_partner" class="form-horizontal" role="form">
   <div class="box-body" style="background-color: #fff;">
         <div class="form-group"><label class="col-sm-2 control-label">Name of Partner: </label>
         <div class="col-sm-4"><input type="text" name="name">
         </div>
         </div>
         </div>
         <div class="box-footer">
          <input type="submit" class="btn btn-primary" name="SUBMIT" value="SUBMIT">
          </div>
       </form>
</section>
<br>

<!-- ============================================================= -->

<section id="delete-partner">
  <h2 class="page-header"><a href="#delete-partner">Delete Existing Partner</a></h2>
  <form method="post" action="records/delete_existing_partner" class="form-horizontal" role="form">
   <div class="box-body" style="background-color: #fff;">
         <div class="form-group"><label class="col-sm-2 control-label">Delete Partner:  </label>
         <div class="col-sm-4"><select name='name'>
         <?php foreach ($par as $key => $value) { 
           echo"<option value=$value[name]>$value[name]</option>";
            } ?>
         </select>
         </div>
         </div>
         </div>
         <div class="box-footer">
         <input type="submit" class="btn btn-primary" name="submit" value="SUBMIT">
         </div>
       </form>
</section>
<br>

<!-- ============================================================= -->

<section id="new-testimonial">
  <h2 class="page-header"><a href="#new-testimonial">Add New Testimonial</a></h2>
<form enctype="multipart/form-data" method="post" action="records/add_new_testimonial" class="form-horizontal" role="form">
   <div class="box-body" style="background-color: #fff;">
         <div class="form-group"><label class="col-sm-2 control-label">Name of Testifier: </label>
         <div class="col-sm-10"><input type="text" name="name">
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Address: </label>
         <div class="col-sm-10"><input type="text" name="address">
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Comment: </label>
         <div class="col-sm-10"><input type="text" name="comment">
         </div>
         </div>
         </div>
         <div class="box-footer">
          <input type="submit" class="btn btn-primary" name="SUBMIT" value="SUBMIT">
          </div>
       </form>
</section>
<br>

<!-- ============================================================= -->

<section id="delete-testimonial">
  <h2 class="page-header"><a href="#delete-testimonial">Delete Testimonial</a></h2>
  <form method="post" action="records/delete_existing_testimonial" class="form-horizontal" role="form">
   <div class="box-body" style="background-color: #fff;">
         <div class="form-group"><label class="col-sm-2 control-label">Delete Testimonial:  </label>
         <div class="col-sm-4"><select name='name'>
         <?php foreach ($test as $key => $value) { 
           echo"<option value='$value[name]'>$value[name]</option>";
            } ?>
         </select>
         </div>
         </div>
         </div>
         <div class="box-footer">
         <input type="submit" class="btn btn-primary" name="submit" value="SUBMIT">
         </div>
       </form>
</section>
<br>

<!-- ============================================================= -->

<section id="new-service">
  <h2 class="page-header"><a href="#new-service">Add New Service</a></h2>
 <form method="post" action="records/add_new_service" class="form-horizontal" role="form">
   <div class="box-body" style="background-color: #fff;">
         <div class="form-group"><label class="col-sm-2 control-label">Service: </label>
         <div class="col-sm-10"><input type="text" name="title">
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Details: </label>
         <div class="col-sm-10"><input type="text" name="service">
         </div>
         </div>
         </div>
         <div class="box-footer">
          <input type="submit" class="btn btn-primary" name="SUBMIT" value="SUBMIT">
          </div>
       </form>
</section>
<br>

<!-- ============================================================= -->

<section id="delete-service">
  <h2 class="page-header"><a href="#delete-service">Remove Existing Service</a></h2>
  <form method="post" action="records/delete_existing_service" class="form-horizontal" role="form">
   <div class="box-body" style="background-color: #fff;">
         <div class="form-group"><label class="col-sm-2 control-label">Delete Service:  </label>
         <div class="col-sm-4"><select name='title'>
         <?php foreach ($ser as $key => $value) { 
           echo"<option value='$value[title]'>$value[title]</option>";
            } ?>
         </select>
         </div>
         </div>
         </div>
         <div class="box-footer">
         <input type="submit" class="btn btn-primary" name="submit" value="SUBMIT">
         </div>
       </form>


<!-- ============================================================= -->

<section id="new-term">
  <h2 class="page-header"><a href="#new-term">Add New Term</a></h2>
 <form method="post" action="records/add_new_term" class="form-horizontal" role="form">
   <div class="box-body" style="background-color: #fff;">
         <div class="form-group"><label class="col-sm-2 control-label">Term: </label>
         <div class="col-sm-10"><input type="text" name="term">
         </div>
         </div>
         <div class="form-group"><label class="col-sm-2 control-label">Details: </label>
         <div class="col-sm-10"><input type="text" name="details">
         </div>
         </div>
         </div>
         <div class="box-footer">
          <input type="submit" class="btn btn-primary" name="SUBMIT" value="SUBMIT">
          </div>
       </form>
</section>
<br>

<!-- ============================================================= -->

<section id="delete-term">
  <h2 class="page-header"><a href="#delete-term">Remove Existing Term</a></h2>
  <form method="post" action="records/delete_existing_term" class="form-horizontal" role="form">
   <div class="box-body" style="background-color: #fff;">
         <div class="form-group"><label class="col-sm-2 control-label">Delete Term:  </label>
         <div class="col-sm-4"><select name='term'>
         <?php foreach ($ter as $key => $value) { 
           echo"<option value='$value[term]'>$value[term]</option>";
            } ?>
         </select>
         </div>
         </div>
         </div>
         <div class="box-footer">
         <input type="submit" class="btn btn-primary" name="submit" value="SUBMIT">
         </div>
       </form>
</section>
        </div><!-- /.content -->
      </div><!-- /.content-wrapper -->