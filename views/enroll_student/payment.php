        <!-- Start Page Banner -->
    <div class="page-banner" style="padding:40px 0; background: url(public/images/slide-02-bg.jpg) center #f9f9f9;">
      <div class="container">
        <div class="row">
          <div class="col-md-12" style="text-align:center;">
            <h2>Congratulations</h2>
             <h3> You are now a registered student of Tentacular academy</h3>
          </div>
         
        </div>
      </div>
    </div>
    <!-- End Page Banner -->
<!-- Start Content -->
    <div id="content">
      <div class="container">

        <div class="row">

          

          <div class="col-md-12">


            <!-- Classic Heading -->
            <h4 class="classic-title"><span>Benefits of being a student </span></h4>
            <!-- Info - Icons List -->
            <ul class="icons-list">
              
              <li><i class="fa fa-info"></i> <strong>Discount:</strong> Every student gets a 10% discount on every tentacular parckage </li>
              <li><i class="fa fa-info"></i> <strong>Free Mentorship:</strong>Send us your coding challenges and we would help you solve them for free</li>
                <li><i class="fa fa-info"></i> <strong>Create a project:</strong>Create a project and get other student join you on the project for free </li>
                <li><i class="fa fa-info"></i>  <strong>Join our Challenges:</strong> Our weekly challenges that helps you build your skill </li>
                <li><i class="fa fa-info"></i> <strong>Free materials:</strong> Download free tentacular parterials based on the program you chose. and lots more </li>
                <li><i class="fa fa-info"></i> <strong>Thanks:</strong> Please Login to your dashboard to view and download materials for the training <a href="<?php echo URL; ?>student_login">Click Here To Login</a> </li>
              
            </ul>

          </div>

        </div>

      </div>
    </div>
    <!-- End content -->

  <script type="text/javascript" src="public/js/script.js"></script>

  <script type="text/javascript">
    //Registration Form

    $('#submit').click(function() {

      $.post("codeless/register", $(".registration-form").serialize(), function(response) {
        $('#success').html(response);
      });
      return false;

    });
  </script>

</body>

</html>