        <!-- Start Page Banner -->
    <div class="page-banner" style="padding:40px 0; background: url(public/images/slide-02-bg.jpg) center #f9f9f9;">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2>New Student Registration</h2>
          </div>
          <div class="col-md-6">
            <ul class="breadcrumbs">
              <li><a href="<?php echo URL?>index">Home</a></li>
              <li><a href="<?php echo URL?>academy">Academy</a></li>
              <li>Register Student</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page Banner -->
<!-- Start Content -->
    <div id="content">
      <div class="container">

        <div class="row">

          <div class="col-md-8">

            <!-- Classic Heading -->
            <h4 class="classic-title"><span>New Student Registration</span></h4>

            <!-- Start Contact Form -->
            <form  class="registration-form" enctype="multipart/form-data" id="contact-form" method="post" action="">

              <div class="form-group">
                <div class="controls">
                 <input type="text" class="requiredField" name="plan" value="<?php echo base64_decode(urldecode($_GET['plan'])); ?>" readonly>
                </div>
              </div>

              <div class="form-group">
                <div class="controls">
                <select name="course" class="form-control">
                <option value="" selected="selected">Select Course...</option>
                <option value="Web Design">Web Design</option>
                <option value="Web Development">Web Development</option>
                <option value = "Graphics Design">Graphics Design</option>
                <?php if ((base64_decode(urldecode($_GET['plan']))) == 'Advanced') { echo "<option value = 'Online Marketing'>Online Marketing</option>"; } ?>
                </select>
                </div>
              </div>

              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="Full name" name="fullname">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="email" class="email" placeholder="Email" name="email">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="text" class="requiredField" placeholder="Username" name="username">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="password" class="requiredField  form-control" placeholder="Password not more than 5 characters" size="30" name="password">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="text" class="requiredField" placeholder="Phone number" name="phone">
                </div>
              </div>

              <div class="form-group">
                <div class="controls">
                  <textarea rows="2" placeholder="Current Home Address" name="address"></textarea>
                </div>
              </div>
              <input type="submit" id="submit" name="sends" class="btn-system btn-large" value="REGISTER">
              
            </form>
            <!-- End Contact Form -->

          </div>

          <div class="col-md-4">

 <?php foreach ($plan as $key => $value) { ?>
            <!-- Classic Heading -->
            <h4 class="classic-title"><span>Course: <?php echo $value['plan'] ?></span></h4>
            <!-- Info - Icons List -->
            <ul class="icons-list">
              <li><i class="fa fa-info">  </i> <strong>Price:</strong> N<?php echo $value['price'] ?>.00 
</li>
              <li><i class="fa fa-info"></i> <strong>Duration:</strong> <?php echo $value['duration'] ?> Month(s)</li>
              <li><i class="fa fa-info"></i> <strong>Packages:</strong> <?php echo $value['packages'] ?></li>
              <li><i class="fa fa-info"></i> <strong>Days of the week:</strong> <?php echo $value['weekdays'] ?></li>
            </ul>
<?php } ?>
          </div>

        </div>

      </div>
    </div>
    <!-- End content -->

  <script type="text/javascript" src="public/js/script.js"></script>

  <script type="text/javascript">
    //Registration Form

    $('#submit').click(function() {

      $.post("enroll_student/register", $(".registration-form").serialize(), function(response) {
        $('#success').html(response);
        window.location.href = "./payment";
      });
      return false;

    });
  </script>

</body>

</html>