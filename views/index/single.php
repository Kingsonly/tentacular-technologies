    <!-- Start Page Banner -->
    <div class="page-banner">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2><?php foreach ($pick as $key => $value) { ?><?php echo $value['job_name'] ?><?php } ?></h2>
          </div>
          <div class="col-md-6">
            <ul class="breadcrumbs">
              <li><a href="<?php echo URL?>index">Home</a></li>
              <li><a href="<?php echo URL?>portfolio">Portfolio</a></li>
              <li>Single Project</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page Banner -->


    <!-- Start Content -->
    <div id="content">
      <div class="container">
        <div class="project-page row">

       <?php foreach ($pick as $key => $value) { ?>
          <!-- Start Single Project Slider -->
          <div class="project-media col-md-8">
            <div class="touch-slider project-slider">
              <div class="item">
                <a class="lightbox" title="" href="public/images/<?php echo $value['pix_folder'] ?>/1.png" data-lightbox-gallery="gallery2">
                  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                  <img alt="" src="public/images/<?php echo $value['pix_folder'] ?>/1.png" style="width:100%;">
                </a>
              </div>
              <div class="item">
                <a class="lightbox" title="" href="public/images/<?php echo $value['pix_folder'] ?>/2.png" data-lightbox-gallery="gallery2">
                  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                  <img alt="" src="public/images/<?php echo $value['pix_folder'] ?>/2.png" style="width:100%;">
                </a>
              </div>
              <div class="item">
                <a class="lightbox" title="" href="public/images/<?php echo $value['pix_folder'] ?>/3.png" data-lightbox-gallery="gallery2">
                  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                  <img alt="" src="public/images/<?php echo $value['pix_folder'] ?>/3.png" style="width:100%;">
                </a>
              </div>
            </div>
          </div>
          <!-- End Single Project Slider -->

          <!-- Start Project Content -->
          <div class="project-content col-md-4">
            <h4><span>Project Description</span></h4>
            <p><?php echo $value['job_desc'] ?></p>
            <h4><span>Project Details</span></h4>
            <ul>
              <li><strong>Job Title: </strong><?php echo $value['job_name'] ?></li>
              <li><strong>Client: </strong><?php echo $value['client'] ?></li>
              <li><strong>Date Embarked: </strong><?php echo $value['date'] ?></li>
              <li><strong>Skills: </strong> <?php echo $value['skills'] ?></li>
              <li><strong>Status: </strong><?php echo $value['status'] ?></li>
            </ul>
            <div class="post-share">
              <span>Share This:</span>
              <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
              <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
              <a class="gplus" href="#"><i class="fa fa-google-plus"></i></a>
              <a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a>
              <a class="mail" href="#"><i class="fa fa-envelope"></i></a>
            </div>
          </div>
          <!-- End Project Content -->

        </div><?php } ?>

        
        <!-- Start Recent Projects Carousel -->
        <div class="recent-projects">
          <h4 class="title"><span>Recent Projects</span></h4>
          <div class="projects-carousel touch-carousel">

       <?php foreach ($port as $key => $value) { ?>
            <div class="portfolio-item item">
              <div class="portfolio-border">
                <div class="portfolio-thumb">
                  <a class="lightbox" href="<?php echo URL.'public/'.'images/portfolio/'.$value['pix']?>">
                    <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                    <img alt="" src="<?php echo URL.'public/'.'images/portfolio/'.$value['pix']?>" style="height: 210px; width: 220px;" />
                  </a>
                </div>
                <div class="portfolio-details">
                  <a href="#">
                    <h4><?php echo $value['client'] ?></h4>
                    <span><?php echo $value['skills'] ?></span>
                  </a>
                </div>
              </div>
            </div>
<?php } ?>
          </div>
        </div>
        <!-- End Recent Projects Carousel -->

      </div>
    </div>
    <!-- End Content -->