    <!-- Start Page Banner -->
    <div class="page-banner">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2><?php foreach ($pick as $key => $value) { ?><?php echo $value['project_name'] ?><?php } ?></h2>
          </div>
          <div class="col-md-6">
            <ul class="breadcrumbs">
              <li><a href="<?php echo URL?>index">Home</a></li>
              <li><a href="<?php echo URL?>portfolio">Portfolio</a></li>
              <li>Academy Projects</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page Banner -->


    <!-- Start Content -->
    <div id="content">
      <div class="container">
        <div class="project-page row">

          <!-- Start Single Project Slider -->
          <div class="project-media col-md-8">
            <div class="touch-slider project-slider">
              <div class="item">
                <a class="lightbox" title="This is an image title" href="public/images/portfolio-1/1.png" data-lightbox-gallery="gallery2">
                  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                  <img alt="" src="public/images/portfolio-1/1.png" style="width:100%;">
                </a>
              </div>
              <div class="item">
                <a class="lightbox" title="This is an image title" href="public/images/portfolio-1/4.png" data-lightbox-gallery="gallery2">
                  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                  <img alt="" src="public/images/portfolio-1/4.png" style="width:100%;">
                </a>
              </div>
              <div class="item">
                <a class="lightbox" title="This is an image title" href="public/images/portfolio-1/12.png" data-lightbox-gallery="gallery2">
                  <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                  <img alt="" src="public/images/portfolio-1/12.png" style="width:100%;">
                </a>
              </div>
            </div>
          </div>
          <!-- End Single Project Slider -->

       <?php foreach ($pick as $key => $value) { ?>
          <!-- Start Project Content -->
          <div class="project-content col-md-4">
            <h4><span>Project Description</span></h4>
            <p><?php echo $value['project_desc'] ?></p>
            <h4><span>Project Details</span></h4>
            <ul>
              <li><strong>Project Name: </strong><?php echo $value['project_name'] ?></li>
              <li><strong>Students Involved: </strong><?php echo $value['students'] ?></li>
              <li><strong>Date Embarked: </strong><?php echo $value['date'] ?></li>
              <li><strong>Skills: </strong> <?php echo $value['skills'] ?></li>
              <li><strong>Status: </strong><?php echo $value['status'] ?></li>
            </ul>
            <div class="post-share">
              <span>Share This:</span>
              <a class="facebook" href="#"><i class="fa fa-facebook"></i></a>
              <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
              <a class="gplus" href="#"><i class="fa fa-google-plus"></i></a>
              <a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a>
              <a class="mail" href="#"><i class="fa fa-envelope"></i></a>
            </div>
          </div>
          <!-- End Project Content -->
<?php } ?>
        </div>

      </div>
    </div>
    <!-- End Content -->