    <!-- Start Home Page Slider -->
    <section id="home">
      <!-- Carousel -->
      <div id="main-slide" class="carousel slide" data-ride="carousel">

        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#main-slide" data-slide-to="0" class="active"></li>
          <li data-target="#main-slide" data-slide-to="1"></li>
          <li data-target="#main-slide" data-slide-to="2"></li>
        </ol>
        <!--/ Indicators end-->

        <!-- Carousel inner -->
        <div class="carousel-inner">
          <div class="item active">
            <img class="img-responsive" src="public/images/slider/bg1.jpg" alt="slider">
            <div class="slider-content">
              <div class="col-md-12 text-center">
                <h2 class="animated2">
                              <span><strong>Tentacular </strong>Technologies</span>
                              </h2>
                <h3 class="animated3">
                                <span>Creativity meets Innovation!</span>
                              </h3>
              </div>
            </div>
          </div>
          <!--/ Carousel item end -->
          <div class="item">
            <img class="img-responsive" src="public/images/slider/bg2.png" alt="slider">
            <div class="slider-content">
              <div class="col-md-12 text-center">
                <h2 class="animated4">
                                <span><strong>Tentacular</strong> Academy</span>
                            </h2>
                <h3 class="animated5">
                              <span style="color: #fff;">The Key to your Success</span>
                            </h3>
                <p class="animated6"><a href="<?php echo URL?>academy" class="slider btn btn-system btn-large">ENROLL TODAY</a>
                </p>
              </div>
            </div>
          </div>
          <!--/ Carousel item end -->
          <div class="item">
            <img class="img-responsive" src="public/images/slider/bg3.jpg" alt="slider">
            <div class="slider-content">
              <div class="col-md-12 text-center">
                <h2 class="animated7 white">
                                <span>Tentacular<strong> E-Solutions</strong></span>
                            </h2>
                <h3 class="animated8 white">
                              <span>Speed, Access & Useability</span>
                            </h3>
              </div>
            </div>
          </div>
          <!--/ Carousel item end -->
        </div>
        <!-- Carousel inner end-->

        <!-- Controls -->
        <a class="left carousel-control" href="#main-slide" data-slide="prev">
          <span><i class="fa fa-angle-left"></i></span>
        </a>
        <a class="right carousel-control" href="#main-slide" data-slide="next">
          <span><i class="fa fa-angle-right"></i></span>
        </a>
      </div>
      <!-- /carousel -->
    </section>
    <!-- End Home Page Slider -->


    <!-- Start Services Section -->
    <div class="section service">
      <div class="container">
        <div class="row">


     <!-- Start Big Heading -->
      <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
        <h1><strong>Services</strong> We Offer</h1>
      </div>
      <!-- End Big Heading -->
 <?php foreach ($ser as $key => $value) { ?>
          <!-- Start Service Icon 1 -->
          <div class="col-md-3 col-sm-6 service-box service-center" data-animation="fadeIn" data-animation-delay="01">
            <div class="service-icon">
              <i class="fa fa-leaf icon-large"></i>
            </div>
            <div class="service-content">
              <h4><?php echo $value['title'] ?></h4>

            </div>
          </div>
          <!-- End Service Icon 1 -->

            <?php } ?>

        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Services Section -->


    <!-- Start Purchase Section -->
    <div class="section purchase">
      <div class="container">

        <!-- Start Video Section Content -->
        <div class="section-video-content text-center">

          <!-- Start Animations Text --> <div style="background-color: #000; opacity: 0.8; border-radius: 15px;">
          <h1 class="fittext red-text tlt">
                      <span class="texts">
                        <span>Back & Front-end Development</span>
                        <span>Web & Mobile App Development</span>
                        <span>Systems Hardware</span>
                        <span>System Analysis & Design</span>
                        <span>Project Management</span>
                        <span>IT Consulting</span>

                      </span>

                      <br>Tentacular Technologies is ready to handle your IT related businesses
                    </h1></div>
          <!-- End Animations Text -->


          <!-- Start Buttons -->
          <a href="<?php echo URL?>portfolio" class="btn-system btn-large"><i class="fa fa-suitcase"></i> Check Out Our Portfolio</a>
          <a href="<?php echo URL?>contact" class="btn-system btn-large btn-wite"><i class="fa fa-phone"></i> Contact Us</a>

        </div>
        <!-- End Section Content -->

      </div>
      <!-- .container -->
    </div>
    <!-- End Purchase Section -->


    <!-- Start Portfolio Section -->
    <div class="section full-width-portfolio" style="border-top:0; border-bottom:0; background:#fff;">

      <!-- Start Big Heading -->
      <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
        <h1>Excerpts From Our Latest <strong>Works</strong></h1>
      </div>
      <!-- End Big Heading -->

      <!-- Start Recent Projects Carousel -->
      <ul class="row" id="portfolio-list" data-animated="fadeIn">
       <?php foreach ($port as $key => $value) { ?>
        <li class="col-md-3 col-sm-6">
          <img src="<?php echo URL.'public/'.'images/portfolio/'.$value['pix']?>" style="height: 230px; width: 250px;" alt="" />
          <div class="portfolio-item-content">
            <span class="header"><?php echo $value['client'] ?></span>
            <p class="body"><?php echo $value['skills'] ?></p>
          </div>
          <a href="<?php echo URL?>single?pick=<?php echo urlencode(base64_encode($value['portfolio_id'])) ?>"><i class="more">+</i></a>

        </li>
        <?php } ?>
      </ul>

      <!-- End Recent Projects Carousel -->


    </div>
    <!-- End Portfolio Section -->


    <!-- Start Team Member Section -->
    <div class="section" style="background:#fff;">
      <div class="container">

        <!-- Start Big Heading -->
        <div class="big-title text-center" data-animation="fadeInDown" data-animation-delay="01">
          <h1>Our Great <strong>Team</strong></h1>
        </div>
        <!-- End Big Heading -->

        <!-- Some Text -->
        <p class="text-center">Our most-efficient team of programmers, mentors, network administrators and other IT experts work expressely to deliver solutions to you in the best possible way.</p>


        <!-- Start Team Members -->
        <div class="row">

 <?php foreach ($team as $key => $value) { ?>
          <!-- Start Memebr 1 -->
          <div  class="col-md-4 col-sm-6 col-xs-12" data-animation="fadeIn" data-animation-delay="03">
            <div class="team-member modern">
              <!-- Memebr Photo, Name & Position -->
              <div class="member-photo">
                <img alt="" src="<?php echo URL.'public/'.'images/team/'.$value['pix']?>" style="height: 250px; width: 260px;"/>
                <div class="member-name"><?php echo $value['name'] ?><span><?php echo $value['position'] ?></span>
                </div>
              </div>
              <!-- Start Progress Bar 1 -->
              <div class="progress-label"><?php echo $value['skills'] ?></div>
              
              
            </div>
          </div>
          <!-- End Memebr 1 -->
<?php } ?>
        </div>
        <!-- End Team Members -->

      </div>
      <!-- .container -->
    </div>
    <!-- End Team Member Section -->


    <div id="parallax-one" class="parallax" style="background-image:url(public/images/parallax/bg-02.jpg);">
      <div class="parallax-text-container-1">
        <div class="parallax-text-item">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="counter-item">
                  <i class="fa fa-cloud-upload"></i>
                  <div class="timer" id="item1" data-to="991" data-speed="5000"></div>
                  <h5>Database Queries</h5>
                </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="counter-item">
                  <i class="fa fa-check"></i>
                  <div class="timer" id="item2" data-to="94" data-speed="5000"></div>
                  <h5>Projects completed</h5>
                </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="counter-item">
                  <i class="fa fa-code"></i>
                  <div class="timer" id="item3" data-to="18745" data-speed="5000"></div>
                  <h5>Lines of code written</h5>
                </div>
              </div>
              <div class="col-xs-12 col-sm-3 col-md-3">
                <div class="counter-item">
                  <i class="fa fa-male"></i>
                  <div class="timer" id="item4" data-to="53" data-speed="5000"></div>
                  <h5>Happy clients</h5>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <!-- Start Pricing Table Section -->
    <div class=" section pricing-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <!-- Start Big Heading -->
            <div class="big-title text-center">
            <h2>Welcome To The <strong>Tentacular Academy</strong></h2>
              <h1>We Have Nice Pricing Plans For <strong>You!</strong></h1>
            </div>
            <!-- End Big Heading -->

            <!-- Text -->
            <p class="text-center">The &copy; Tenttacular Academy offers provisional tutoring classes on a wide range of courses. We also create job opportunities for our best students with exceptional skills. <span class="accent-color sh-tooltip">Enroll Today</span> and let us equip you with the right tools, skills and materials to ensure your IT credibility.</p>
          </div>
        </div>

        <div class="row pricing-tables">

 <?php foreach ($plan as $key => $value) { ?>
          <div class="col-md-4 col-sm-4 col-xs-12" >
            <div class="pricing-table highlight-plan">
              <div class="plan-name">
                <h3><?php echo $value['plan'] ?></h3>
              </div>
              <div class="plan-price">
                <div class="price-value">N<?php echo $value['price'] ?><span>.00</span></div>
                <div class="interval" style="color:red;font-size:16px;">Registration:Free</div>
              </div>
              <div class="plan-list">
                <ul>
                  <li><strong>Duration: <?php echo $value['duration'] ?></strong> Month(s)</li>
                  <li style="height:47px;">Packages: <strong><?php echo $value['packages'] ?></strong></li>
                  <li>Days of the Week: <strong><?php echo $value['weekdays'] ?></strong></li>
                </ul>
              </div>
              <div class="plan-signup">
                 <a href="<?php if($value['plan']=='professional'){echo 'codeless';}else{echo 'enroll_student?plan='.urlencode(base64_encode($value['plan']));}?>" class="btn-system btn-small border-btn">Sign Up Now</a>
              </div>
            </div>
          </div>
<?php } ?>

        </div>
      </div>
    </div>
    <!-- End Pricing Table Section -->


    <!-- Start Client/Partner Section -->
    <div class="partner">
      <div class="container">
        <div class="row">

          <!-- Start Big Heading -->
          <div class="big-title text-center">
            <h1>Why not <strong>Partner</strong> with Us?</h1>
          </div>
          <!-- End Big Heading -->

          <!--Start Clients Carousel-->
          <div class="our-clients">
            <div class="clients-carousel custom-carousel touch-carousel navigation-3" data-appeared-items="5" data-navigation="true">

 <?php foreach ($part as $key => $value) { ?>
              <!-- Client 1 -->
              <div class="client-item item">
                <a href="#"><img src="<?php echo URL.'public/'.'images/partners/'.$value['pix']?>" alt="" /></a>
              </div>
<?php } ?>
            </div>
          </div>
          <!-- End Clients Carousel -->
        </div>
        <!-- .row -->
      </div>
      <!-- .container -->
    </div>
    <!-- End Client/Partner Section -->

