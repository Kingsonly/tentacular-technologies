    <!-- Start Page Banner -->
    <div class="page-banner" style="padding:40px 0; background: url(public/images/slide-02-bg.jpg) center #f9f9f9;">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2>About Us</h2>
            <p>We Are Professional</p>
          </div>
          <div class="col-md-6">
            <ul class="breadcrumbs">
              <li><a href="<?php echo URL?>index">Home</a></li>
              <li>About Us</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page Banner -->


    <!-- Start Content -->
    <div id="content">
      <div class="container">
        <div class="page-content">


          <div class="row">

            <div class="col-md-12">

              <!-- Classic Heading -->
              <h4 class="classic-title"><span>Welcome To Our Agency</span></h4>

              <!-- Some Text -->
              <p><a title="Simple Tooltip" href="#" class="itl-tooltip">Tentacular Technologies™ </a> is an organization based and built on Information technology. It focuses on providing I.T solutions to individuals and companies form small to large scale.</p>
              <p>It was founded in 2014 and has ever since, provided the best of services to its clients in the most efficient time and most affordable way. Our center focus is on creativity and innovation and how we can use these alongside our vision to make create a better standard of living for our clients and all around us.</p>

            </div>

          </div>

          <!-- Divider -->
          <div class="hr1" style="margin-bottom:50px;"></div>

          <div class="row">
          <div class="col-md-4">

              <!-- Classic Heading -->
              <h4 class="classic-title"><span>Our Vision</span></h4>
              <p>Solving human problems and growing of wealth with the use of technology </p>

              <!-- Classic Heading -->
              <h4 class="classic-title"><span>Our Mission Statement</span></h4>
            <p>Leading Africa to becoming a software heaven. </p>

            </div>
            <div class="col-md-4">

              <!-- Classic Heading -->
              <h4 class="classic-title"><span>App Development Skills</span></h4>

              <div class="skill-shortcode">
                <div class="skill">
                  <p>PYTHON</p>
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" data-percentage="100">
                      <span class="progress-bar-span">100%</span>
                      <span class="sr-only">100% Complete</span>
                    </div>
                  </div>
                </div>
                <div class="skill">
                  <p>CSS 3/ HTML 5</p>
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" data-percentage="100">
                      <span class="progress-bar-span">100%</span>
                      <span class="sr-only">100% Complete</span>
                    </div>
                  </div>
                </div>
                <div class="skill">
                  <p>PHP/ MySQLi</p>
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" data-percentage="100">
                      <span class="progress-bar-span">100%</span>
                      <span class="sr-only">100% Complete</span>
                    </div>
                  </div>
                </div>
                <div class="skill">
                  <p>JavaScript/ AJAX</p>
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" data-percentage="100">
                      <span class="progress-bar-span">100%</span>
                      <span class="sr-only">100% Complete</span>
                    </div>
                  </div>
                </div>
                <div class="skill">
                  <p>jQuery Mobile</p>
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" data-percentage="100">
                      <span class="progress-bar-span">100%</span>
                      <span class="sr-only">100% Complete</span>
                    </div>
                  </div>
                </div>
              </div>

            </div>

            <div class="col-md-4">

              <!-- Classic Heading -->
              <h4 class="classic-title"><span>Gallery</span></h4>
              <!-- Start Touch Slider -->
              <div class="touch-slider" data-slider-navigation="true" data-slider-pagination="true">
                <div class="item"><img style="height: 200px;" alt="" src="public/images/slider/bg1.jpg"></div>
                <div class="item"><img style="height: 200px;" alt="" src="public/images/slider/bg2.png"></div>
                <div class="item">
                    <img style=" height:200px;" alt="" src="public/images/slider/bg3.jpg" >
                  </div>
              </div>
              <!-- End Touch Slider -->

            </div>

            </div>

          <!-- Divider -->
          <div class="hr1" style="margin-bottom:50px;"></div>

            <div class="col-md-12">

              <!-- Classic Heading -->
              <h4 class="classic-title"><span>Our Core Values</span></h4>
                <div class="row">
                <!-- Start Service Icon 1 -->
                <div class="col-md-4 service-box service-icon-left-more">
                  <div class="service-icon">
                    <i class="fa fa-magic icon-medium"></i>
                  </div>
                  <div class="service-content">
                    <h4>Creativity</h4>
                  </div>
                </div>

                <div class="col-md-4 service-box service-icon-left-more">
                  <div class="service-icon">
                    <i class="fa fa-leaf icon-medium"></i>
                  </div>
                  <div class="service-content">
                    <h4>Integrity</h4>
                  </div>
                </div>
                
                <div class="col-md-4 service-box service-icon-left-more">
                  <div class="service-icon">
                    <i class="fa fa-umbrella icon-medium"></i>
                  </div>
                  <div class="service-content">
                    <h4>Stakeholder Commitment</h4>
                  </div>
                </div>

                <div class="col-md-4 service-box service-icon-left-more">
                  <div class="service-icon">
                    <i class="fa fa-magic icon-medium"></i>
                  </div>
                  <div class="service-content">
                    <h4>Resourcefulness</h4>
                  </div>
                </div>
                
                <div class="col-md-4 service-box service-icon-left-more">
                  <div class="service-icon">
                    <i class="fa fa-leaf icon-medium"></i>
                  </div>
                  <div class="service-content">
                    <h4>Time Management</h4>
                  </div>
                </div>
                
                
                <div class="col-md-4 service-box service-icon-left-more">
                  <div class="service-icon">
                    <i class="fa fa-users icon-medium"></i>
                  </div>
                  <div class="service-content">
                    <h4>Synergy</h4>
                  </div>
                </div>
                

                <!-- End Service Icon 1 -->
        
</div>
          </div>

          <!-- Divider -->
          <div class="hr1" style="margin-bottom:50px;"></div>

          <!-- Classic Heading -->
          <h4 class="classic-title"><span>Our Creative Team</span></h4>

          <!-- Start Team Members -->
          <div class="row">

           <?php foreach ($team as $key => $value) { ?>
          <!-- Start Memebr 1 -->
              <div class="col-md-3 col-sm-6 col-xs-12" data-animation="fadeIn" data-animation-delay="03">
                <div class="team-member modern">
                  <!-- Memebr Photo, Name & Position -->
                  <div class="member-photo">
                    <img alt="" src="<?php echo URL.'public/'.'images/team/'.$value['pix']?>" style="height: 250px; width: 260px;"/>
                    <div class="member-name"><?php echo $value['name'] ?><span><?php echo $value['position'] ?></span>
                    </div>
                  </div>
                  <!-- Start Progress Bar 1 -->
                  <div class="progress-label"><?php echo $value['skills'] ?></div>

                  <div class="member-socail">
                    <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                    <a class="gplus" href="#"><i class="fa fa-google-plus"></i></a>
                    <a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                    <a class="flickr" href="#"><i class="fa fa-flickr"></i></a>
                    <a class="mail" href="#"><i class="fa fa-envelope"></i></a>
                  </div>
                </div>
              </div>
          <!-- End Memebr 1 -->
         <?php } ?>

          </div>
          <!-- End Team Members -->

          <!-- Divider -->
          <div class="hr1" style="margin-bottom:50px;"></div>

          <!-- Start Clients Carousel -->
          <div class="our-clients">

            <!-- Classic Heading -->
            <h4 class="classic-title"><span>Our Happy Clients</span></h4>

            <div class="clients-carousel custom-carousel touch-carousel" data-appeared-items="4">

             <?php foreach ($cli as $key => $value) { ?>
              <!-- Client 1 -->
              <div class="client-item item">
                <a href="#"><img style="height:140px;" src="<?php echo URL.'public/'.'images/portfolio/'.$value['pix']?>" alt="" /></a>
              </div>
<?php } ?>

            </div>
          </div>
          <!-- End Clients Carousel -->


        </div>
      </div>
    </div>
    <!-- End content -->

