    <!-- Start Page Banner -->
    <div class="page-banner">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2>Tentacular Academy</h2>
          </div>
          <div class="col-md-6">
            <ul class="breadcrumbs">
              <li><a href="<?php echo URL?>index">Home</a></li>
              <li>Academy</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page Banner -->


    <!-- Start Content -->
    <div id="content">
      <div class="container">

        <!-- Start Services Icons -->
        <div class="row">

          <!-- Start Service Icon 1 -->
          <div class="col-md-3 col-sm-6 service-box service-center">
            <div class="service-boxed">
              <div class="service-icon" style="margin-top:-25px;">
                <i class="fa fa-code icon-medium-effect icon-effect-2"></i>
              </div>
              <div class="service-content">
                <h4>Web Development</h4>
                <p>Web developers design, build and maintain websites and web applications...If you've ever thought about becoming a web developer, you came to the right place! </p>
              </div>
            </div>
          </div>
          <!-- End Service Icon 1 -->

          <!-- Start Service Icon 2 -->
          <div class="col-md-3 col-sm-6 service-box service-center">
            <div class="service-boxed">
              <div class="service-icon" style="margin-top:-25px;">
                <i class="fa fa-code icon-medium-effect icon-effect-2"></i>
              </div>
              <div class="service-content">
                <h4>Mobile</h4>
                <p>If there's one evergreen job title for software developers that just keeps getting greener and greener, it would be Mobile App Developer.</p>
              </div>
            </div>
          </div>
          <!-- End Service Icon 2 -->

          <!-- Start Service Icon 3 -->
          <div class="col-md-3 col-sm-6 service-box service-center">
            <div class="service-boxed">
              <div class="service-icon" style="margin-top:-25px;">
                <i class="fa fa-rocket icon-medium-effect icon-effect-1"></i>
              </div>
              <div class="service-content">
                <h4>Networking</h4>
                <p>No one is immune from networking, no matter how well-connected.</p>
              </div>
            </div>
          </div>
          <!-- End Service Icon 3 -->

          <!-- Start Service Icon 4 -->
          <div class="col-md-3 col-sm-6 service-box service-center">
            <div class="service-boxed">
              <div class="service-icon" style="margin-top:-25px;">
                <i class="fa fa-magic icon-medium-effect icon-effect-1"></i>
              </div>
              <div class="service-content">
                <h4>Graphic Design</h4>
                <p>Graphic designers create visual concepts that inspire, inform, and transform.and discover the skills you need to become a graphic designer</p>
              </div>
            </div>
          </div>
          <!-- End Service Icon 4 -->

        </div>
        <!-- End Services Icons -->

        

        <!-- Divider -->
        <div class="hr1 margin-60"></div>


        <div class="row">

        <div class="col-md-4">

              <!-- Classic Heading -->
              <h4 class="classic-title"><span>Welcome To The Academy!</span></h4>

              <!-- Some Text -->
              <p><a title="Simple Tooltip" class="itl-tooltip">Welcome to the Tentacular Academy.</a> Where young and vibrant minds are shaped into productive and innovative assets. Our institution of information technology offers a wide range of courses from web design and development, to graphics and networking. We also run professional programs every 2nd saturday of the month. Enroll today and join the never ending stream of technology experts!</p>

            </div>

          <div class="col-md-4">

            <!-- Classic Heading -->
            <h4 class="classic-title"><span>More About Us</span></h4>

            <!-- Nav Tabs -->
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab1" data-toggle="tab"><i class="icon-award-1"></i>Location</a></li>
              <li><a href="#tab2" data-toggle="tab"><i class="icon-beaker"></i>Time</a></li>
              <li><a href="#tab3" data-toggle="tab"><i class="icon-droplet"></i>Payment Options</a></li>
            </ul>

            <!-- Tab Panels -->
            <div class="tab-content">
              <!-- Tab Content 1 -->
              <div class="tab-pane fade in active" id="tab1">
                  <p>The academy holds all of its classes in the NCCE Annex building, Central Area Abuja at the different appointed times. Please contact us for directions. Also refer to your Academy Curriculum document sent to you after registration.</p>
              </div>
              <!-- Tab Content 2 -->
              <div class="tab-pane fade" id="tab2">
                <p>All of the Academy classes run through 3.30pm - 5.30pm on the appointed days for each package. The Starter package holds every saturday of the week, The Basic package holds on Thursdays and fridays and the Advanced package holds on Mondays, Tuesdays and Wednesdays.</p>
              </div>
              <!-- Tab Content 1 -->
              <div class="tab-pane fade" id="tab3">
                <p>We offer three payment plans for our potential students: </p>
                <li>You can pay directly to the Bank using our account details</li>
                <li>You can pay directly to Us at the Lecture venue</li>
                <li>You can pay online</li><br>
                <p>Also 50% part payment is permitted for the Basic and Advanced packages. The balance 50% however, must be made by within a months duration of one's course.</p>
              </div>
            </div>

          </div>

          <div class="col-md-4">

            <!-- Classic Heading -->
            <h4 class="classic-title"><span>Why Choose Us</span></h4>

            <!-- Accordion -->
            <div class="panel-group" id="accordion">

              <!-- Start Accordion 1 -->
              <div class="panel panel-default">
                <!-- Toggle Heading -->
                <div class="panel-heading">
                  <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse-one">
            <i class="icon-down-open-1 control-icon"></i>
            <i class="icon-laptop-1"></i> Professional IT Tutors on the Go!
        </a>
    </h4>
                </div>
                <!-- Toggle Content -->
                <div id="collapse-one" class="panel-collapse collapse in">
                  <div class="panel-body">We pride ourselves with having specialized instructors who impact knowledge in the best possible way. Our tutors also go all the way to ensure that your learning is to put to real world practice and offer mentorship for interested students.</div>
                </div>
              </div>
              <!-- End Accordion 1 -->

              <!-- Start Accordion 2 -->
              <div class="panel panel-default">
                <!-- Toggle Heading -->
                <div class="panel-heading">
                  <h4 class="panel-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapse-tow" class="collapsed">
         <i class="icon-down-open-1 control-icon"></i>
         <i class="icon-gift-1"></i> Affordable Prices
     </a>
 </h4>
                </div>
                <!-- Toggle Content -->
                <div id="collapse-tow" class="panel-collapse collapse">
                  <div class="panel-body">We offer amazingly affordable prices for all our packages so noone is left out. The academy also allows for part payment for some packages!.</div>
                </div>
              </div>
              <!-- End Accordion 2 -->

              <!-- Start Accordion 3 -->
              <div class="panel panel-default">
                <!-- Toggle Heading -->
                <div class="panel-heading">
                  <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-three" class="collapsed">
          <i class="icon-down-open-1 control-icon"></i>
          <i class="icon-tint"></i> Condusive Learning Environment
      </a>
  </h4>
                </div>
                <!-- Toggle Content -->
                <div id="collapse-three" class="panel-collapse collapse">
                  <div class="panel-body">Our learning environment has been duly structured to ensure that a condusive learning environment is provided for our students, their minds are stimulated to think fast and think smart!.</div>
                </div>
              </div>
              <!-- End Accordion 3 -->

            </div>

          </div>

        </div>
        <!-- Divider -->
        <div class="hr1 margin-top"></div>

         <!-- Start Pricing Table Section -->
    <div class=" section pricing-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <!-- Start Big Heading -->
            <div class="big-title text-center">
            <h2>Enroll <strong>Now!</strong></h2>
              <h1>We Have Amazing Pricing Plans For <strong>You!</strong></h1>
            </div>
            <!-- End Big Heading -->
          </div>
        </div>

        <div class="row pricing-tables">

 <?php foreach ($plan as $key => $value) { ?>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="pricing-table highlight-plan">
              <div class="plan-name">
                <h3><?php echo $value['plan'] ?></h3>
              </div>
              <div class="plan-price">
                <div class="price-value">N<?php echo $value['price'] ?><span>.00</span></div>
                <div class="interval">per month</div>
              </div>
              <div class="plan-list">
                <ul>
                  <li><strong><?php echo $value['duration'] ?></strong> Month(s)</li>
                  <li>Packages: <strong><?php echo $value['packages'] ?></strong></li>
                  <li>Days of the week: <strong><?php echo $value['weekdays'] ?></strong></li>
                </ul>
              </div>
              <div class="plan-signup">
                 <a href="<?php if($value['plan']=='professional'){echo 'codeless';}else{echo 'enroll_student?plan='.urlencode(base64_encode($value['plan']));}?>" class="btn-system btn-small border-btn">Sign Up Now</a>
              </div>
            </div>
          </div>
<?php } ?>

        </div>
      </div>
    </div>
    <!-- End Pricing Table Section -->

        <!-- Divider -->
        <div class="hr1 margin-top"></div>

<!--Start Recent Projects-->

          <div class="recent-projects">
            <h4 class="title"><span>Projects from Our Academy</span></h4>
            <div class="projects-carousel touch-carousel">

       <?php foreach ($portac as $key => $value) { ?>
              <!-- Start Project Item -->
              <div class="portfolio-item item">
                <div class="portfolio-border">
                  <!-- Start Project Thumb -->
                  <div class="portfolio-thumb">
                  <a class="lightbox" href="<?php echo URL.'public/'.'images/academy_projects/'.$value['pix']?>">
                    <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                    <img alt="" src="<?php echo URL.'public/'.'images/academy_projects/'.$value['pix']?>" style="height: 210px; width: 250px;" />
                  </a>
                  </div>
                  <!-- End Project Thumb -->
                  <!-- Start Project Details -->
                  <div class="portfolio-details">
                    <a href="#">
                      <h4><?php echo $value['project_name'] ?></h4>
                      <span><?php echo $value['students'] ?></span>
                    </a>
                  </div>
                  <!-- End Project Details -->
                </div>
              </div>
              <!-- End Project Item -->
              <?php } ?>
              </div>
              </div>
</div>
</div>