

    <!-- Start Page Banner -->
    <div class="page-banner">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2>Portfolio</h2>
          </div>
          <div class="col-md-6">
            <ul class="breadcrumbs">
              <li><a href="<?php echo URL?>index">Home</a></li>
              <li>Portfolio</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page Banner -->


    <!-- Start Content -->
    <div id="content">
      <div class="container">
        <div class=" portfolio-page portfolio-4column">

          <div class="recent-projects">
            <h4 class="title"><span>Our Recent Projects</span></h4>
          <ul id="portfolio-list" data-animated="fadeIn">
       <?php foreach ($port as $key => $value) { ?>
            <li>
              <img src="<?php echo URL.'public/'.'images/portfolio/'.$value['pix']?>" style="height: 210px; width: 220px;" alt="" />
              <div class="portfolio-item-content">
                <span class="header"><?php echo $value['job_name'] ?></span>
                <p class="body"><?php echo $value['skills'] ?></p>
              </div>
              <a href="single?pick=<?php echo urlencode(base64_encode($value['portfolio_id'])) ?>"><i class="more">+</i></a>

            </li>
        <?php } ?>
            </ul>
            </div>
          <!-- End Portfolio Items -->

          <!-- Divider -->
          <div class="hr5" style="margin-top:45px; margin-bottom:35px;"></div>

<!--Start Recent Projects-->
          <div class="recent-projects">
            <h4 class="title"><span>Projects from Our Academy</span></h4>
            <div class="projects-carousel touch-carousel">

       <?php foreach ($portac as $key => $value) { ?>
              <!-- Start Project Item -->
              <div class="portfolio-item item">
                <div class="portfolio-border">
                  <!-- Start Project Thumb -->
                  <div class="portfolio-thumb">
                  <a class="lightbox" href="<?php echo URL.'public/'.'images/academy_projects/'.$value['pix']?>">
                    <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>
                    <img alt="" src="<?php echo URL.'public/'.'images/academy_projects/'.$value['pix']?>" style="height: 210px; width: 250px;" />
                  </a>
                  </div>
                  <!-- End Project Thumb -->
                  <!-- Start Project Details -->
                  <div class="portfolio-details">
                    <a href="#">
                      <h4><?php echo $value['project_name'] ?></h4>
                      <span><?php echo $value['students'] ?></span>
                    </a>
                  </div>
                  <!-- End Project Details -->
                </div>
              </div>
              <!-- End Project Item -->
              <?php } ?>
              </div>
              </div>
        </div>

      </div>
    </div>
    <!-- End Content -->
