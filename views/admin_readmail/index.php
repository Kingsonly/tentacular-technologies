
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Read Mail
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Mailbox</li>
        <li class="active">Read Mail</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-md-3">
          <a href="<?php echo URL?>admin_mailbox" class="btn btn-primary btn-block margin-bottom">Back To Mailbox</a>
</div>
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $_SESSION['msg_name']; ?></h3>
            </div>
            <!-- /.box-header -->
            <?php foreach ($msg_body as $key => $value) { ?>
            <div class="box-body no-padding">
              <div class="mailbox-read-info">
                <h3><?php echo $value['subject'] ?></h3>
                <h5>From: <?php echo $value['email'] ?>
                  <span class="mailbox-read-time pull-right"><?php echo $value['datetime'] ?></span></h5>
              </div>
              
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
 <?php echo $value['message'] ?>
              </div>
              <!-- /.mailbox-read-message -->
            </div>
            <?php } ?>
            <!-- /.box-body -->
            <!-- /.box-footer -->
            <div class="box-footer">
              <div class="pull-right">
              <form action="admin_readmail/update_status" method="send">
                <input type="submit" class="btn btn-default" value="Mark As Read">
                </form>
              </div>
               <form action="admin_readmail/delete_msg" method="send">
                <input type="submit" class="btn btn-default" value="Delete">
                </form>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->