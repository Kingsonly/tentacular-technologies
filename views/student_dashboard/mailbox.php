
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Mailbox
        <small><?php echo $unread ?> new messages</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo URL?>student_dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Mailbox</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            
            <div class="box-header with-border">
              <h3 class="box-title">
                Unread Messages</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">

              <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                <thead>
                  <tr>
                    <td>Sender</td>
                    <td>Subject</td>
                    <td>Message</td>
                    <td>Date</td>
                  </tr>
                </thead>
                  <tbody>
                  <?php foreach ($msg as $key => $value) { ?>
                  <tr>
                    <td class="mailbox-name"><a href="<?php echo URL?>student_readmail?msg_id=<?php echo $value['msg_id']?>"><?php echo $value['sender'] ?></a></td>
                    <td class="mailbox-subject"><b><?php echo $value['subject'] ?></b></td>
                    <td class="mailbox-subject"><b><?php echo $value['message'] ?></b></td>
                    <td class="mailbox-date"><?php echo $value['datetime'] ?></td>
                  </tr>
                  <?php } ?>
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
        </div>
                 <div class="box box-primary">
            
            <div class="box-header with-border">
              <h3 class="box-title">
                Older Messages</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">

              <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                <thead>
                  <tr>
                    <td>Sender</td>
                    <td>Subject</td>
                    <td>Message</td>
                    <td>Date</td>
                  </tr>
                </thead>
                  <tbody>
                  <?php foreach ($oldmsg as $key => $value) { ?>
                  <tr>
                    <td class="mailbox-name"><a href="<?php echo URL?>student_readmail?msg_id=<?php echo $value['msg_id']?>"><?php echo $value['sender'] ?></a></td>
                    <td class="mailbox-subject"><b><?php echo $value['subject'] ?></b></td>
                    <td class="mailbox-subject"><b><?php echo $value['message'] ?></b></td>
                    <td class="mailbox-date"><?php echo $value['datetime'] ?></td>
                  </tr>
                  <?php } ?>
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
        </div>
        <!-- /.col -->
      </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->