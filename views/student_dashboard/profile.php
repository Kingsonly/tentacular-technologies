      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Classmates Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo URL?>student_dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?php echo URL?>classmates">Classmates</a></li>
        <li class="active">Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
          <?php foreach ($profile as $key => $value) { ?>
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-yellow">
              <div class="widget-user-image">
                <img class="img-circle" src="public/images/students/<?php echo $value['pix'] ?>" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username"><?php echo $value['fullname'] ?></h3>
              <h5 class="widget-user-desc"><?php echo $value['course'] ?></h5>
              <h5 class="widget-user-desc"><?php echo $value['date'] ?></h5>
              <?php } ?>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
              <h5><strong>&nbsp;&nbsp;Previous Courses </strong>(If Any)</h5>
          <?php foreach ($academy as $key => $value) { ?>
                <li><a href="#"><?php echo $value['plan']; ?> - <?php echo $value['course']; ?><span class="pull-right badge bg-blue"><?php //echo $value['date']; ?></span></a></li>
            <?php } ?>
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        </div>
        <!-- /.col -->
                <div class="col-md-8">
                <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                Weekly Challenges Timeline</h3>
            </div>
          <!-- The time line -->
          <ul class="timeline">

          <?php foreach ($timeline as $key => $value) { ?>
            <!-- timeline time label -->
            <li class="time-label">
                  <span class="bg-red">
                    <?php echo $value['challenge_date'] ?>
                  </span>
            </li>
            <!-- /.timeline-label -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-envelope bg-blue"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> <?php echo $value['answer_date'] ?></span>

                <div class="timeline-body">
                  <?php echo $value['answer'] ?>
                </div>
                <div class="timeline-footer">
                </div>
              </div>
            </li>
            <!-- END timeline item -->
            <li>
              <i class="fa fa-clock-o bg-gray"></i>
            </li>
            <?php } ?>
          </ul>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      </div>
      
</section>
</div>