
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Read Mail
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo URL?>student_dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?php echo URL?>student_mailbox"><i class="fa fa-envelope"></i> Mailbox</a></li>
        <li class="active">Read Mail</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-md-3">
          <a href="<?php echo URL?>student_mailbox" class="btn btn-primary btn-block margin-bottom">Back To Mailbox</a><br>
</div>
        <div class="col-md-12">
            <?php foreach ($msg_body as $key => $value) { ?>
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                <?php echo $value['subject'] ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-read-info">
                <h5>From: <?php echo $value['sender'] ?>
                  <span class="mailbox-read-time pull-right"><?php echo $value['datetime'] ?></span></h5>
              </div>
              
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
 <?php echo $value['message'] ?>
              </div>
              <!-- /.mailbox-read-message -->
            </div>
            <!-- /.box-body -->
            <!-- /.box-footer -->
            <div class="box-footer">
              <div class="pull-right">
              <form action="student_readmail/update_status" method="send">
                <input type="submit" class="btn btn-default" value="Mark As Read">
                </form>
              </div>
               <form action="student_readmail/reply_msg" method="send">
                <input type="submit" class="btn btn-default" value="Reply">
                </form>
            </div>
            <!-- /.box-footer -->
          </div>
            <?php } ?>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->