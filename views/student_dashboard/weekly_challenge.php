
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Weekly Challenge
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo URL?>student_dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Weekly Challenge</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

          <?php foreach ($challenge as $key => $value) { ?>
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Date</span>
              <span class="info-box-number"><?php echo $value['date']; 
               ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-star"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Title</span>
              <span class="info-box-number"><?php echo $value['name'] ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-files-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">No. Of Paticipants</span>
              <span class="info-box-number"><?php echo $total_answers; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-clock-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Deadline</span>
              <span class="info-box-number"><?php 
              $today = date('Y-m-d').'00-00-00'; 
              if (DATE($value['deadline']) < $today) {
                echo "<br><small>(Closed)</small>";
              } else {
                 echo $value['deadline'];
                } ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-md-9">
          <div class="box box-danger collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Challenge Details</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php echo $value['details'] ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3">
        <?php
              $today = date('Y-m-d').'00-00-00'; 
              if (DATE($value['deadline']) > $today) {
                echo '
        <a class="btn btn-lg btn-danger" data-toggle="modal" data-target="#participate">
                 <font style = "font-variant: small-caps;">Participate</font>
              </a>
              '; } else {
                echo "";
                } ?>
              </div>
        </div>
        <?php } ?>

      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">PARTICIPANTS</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Student</th>
                  <th>Answer</th>
                  <th>Date</th>
                  <th>Rating</th>
                  <th>Admin Rating</th>
                </tr>
                </thead>
                <tbody>
          <?php foreach ($answer as $key => $value) { ?>
                <tr>
                  <td><?php echo $value['student_name'] ?></td>
                  <td><?php echo $value['answer'] ?></td>
                  <td><?php echo $value['answer_date'] ?></td>
                  <td><?php echo $value['ratings'] ?>&nbsp; <a class="btn btn-sm btn-success">
                <i class="fa fa-comments"></i> RATE
              </a></td>
                  <td><?php echo $value['admin_ratings'] ?></td>
                </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Student</th>
                  <th>Answer</th>
                  <th>Date</th>
                  <th>Rating</th>
                  <th>Admin Rating</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<div class="modal fade" id="participate" role = "dialog">
    <div class="modal-dialog modal-lg">
    <div class="modal-content" style="color: #7c7c7c;">
<div class="modal-header">
<h3 class="modal-title" style="color: #e25041; font-variant: small-caps;" align="center">Submit Your Answer</h3>
</div>
<div class="modal-body">
       
            <!-- /.box-header -->
            <div class="box-body pad">
              <form method="POST" action="weekly_challenge/submit_answer">
            
                <input type="hidden" name="student_name" value="<?php echo $fullname ?>">
                <input type="hidden" name="student_id" value="<?php echo $student_id ?>">
          
          <?php foreach ($challenge as $key => $value) { ?>
              
                <input type="hidden" name="challenge_id" value="<?php echo $value['challenge_id'] ?>" >
              
                <input type="hidden" name="challenge_name" value="<?php echo $value['name'] ?>" >
                <input type="hidden" name="challenge_date" value="<?php echo $value['date'] ?>" >
              <?php } ?>
                <textarea class="textarea" placeholder="Type your answer" name="answer" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea><br><br>
                <input type="submit" name="submit" value="SUBMIT" class="btn btn-lg btn-success">
              </form>
            </div>
</div>
<div class="modal-footer">
   <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1');
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
  });
</script>