
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo URL?>student_dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-5">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">

              <h3 class="profile-username text-center"><?php echo $fullname ?></h3>

              <p class="text-muted text-center">Student ID: <?php echo $student_id ?></p>
              
              <img src="public/images/students/<?php echo $pix ?>"  class="profile-user-img img-responsive img-circle" alt="Student Image">
              <center><form enctype="multipart/form-data" method="POST" action="student_dashboard/change_photo">
              <input type="hidden" name="student_id" value="<?php echo $student_id ?>">
                <input type="file" name="photo"><input type="submit" name="submit" value="CHANGE" class="btn btn-danger">
              </form></center><br>


                
                  <b>Address: </b> <a class="pull-right"><?php echo $address ?></a>
              
              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Email: </b> <a class="pull-right"><?php echo $email ?></a>
                </li>
              </ul>
              <b>Phone: </b><a class="pull-right"><?php echo $phone ?></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">ACADEMY DETAILS</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Plan</strong>

              <p class="text-muted">
                <?php echo $plan ?>
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Payment Status</strong>

              <p class="text-muted"><?php echo $paid_status ?> - <?php echo $paid ?></p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Course</strong>

              <p class="text-muted"><?php echo $course; ?></p>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-7">
                  <!-- Box Comment -->
          <!-- /.box -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#make_payment" data-toggle="tab">Make Payment</a></li>
              <li><a href="#request_mentorship" data-toggle="tab">Request Mentorship</a></li>
              <li><a href="#update_profile" data-toggle="tab">Update Profile</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="make_payment">
               <form method="post" action="" class="form-horizontal" id="update-form" role="form">
   <div class="box-body" style="background-color: #fff;">
                <div class="form-group"><label for="payment_method" class="col-sm-4 control-label">Select Payment Method:  </label>
                <div class="col-sm-8"><select name="payment_method" class="form-control">
           <option value="bank">Bank</option>
           <option value="online">Online</option>
         </select>
         </div>
         </div>
                </div>
                <div class="box-footer">

              <div id="success" style="color:#34495e;"></div>
          <input type="submit" class="btn btn-primary" id="submit" name="SUBMIT" value="SELECT">
          </div>
       </form>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="request_mentorship">
                 <form method="post" action="student_dashboard/get_mentor" class="form-horizontal" id="update-form" role="form">
   <div class="box-body" style="background-color: #fff;">
                <div class="form-group"><label for="update_field" class="col-sm-4 control-label">Select Mentor:  </label>
                <div class="col-sm-8">
                <select name="username" class="form-control" style="text-align: center;">
                     <option value="0" selected="selected"><-- Chose A Mentor--></option>
                     <?php foreach ($mentor as $key => $value) { ?>

                       <option value="<?php echo $value['fullname'] ; ?>"><?php echo $value['fullname'];  ?></option>

                       <?php } ?>
                </select>
         </div>
         </div>
                <div class="form-group"><label class="col-sm-4 control-label" for="update">Code:  </label>
                <div class="col-sm-8"><input type="text" name="update" class="form-control">
                </div>
                </div>
                </div>
                <div class="box-footer">

              <div id="success" style="color:#34495e;"></div>
          <input type="submit" class="btn btn-primary" id="submit" name="SUBMIT" value="SUBMIT">
          </div>
       </form>
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="update_profile">
                <form method="post" action="student_dashboard/update_student" class="form-horizontal" id="update-form" role="form">
   <div class="box-body" style="background-color: #fff;">
                <div class="form-group"><label for="update_field" class="col-sm-2 control-label">Update field:  </label>
                <div class="col-sm-10"><select name="update_field" class="form-control">
           
           <option value="password">Password</option>
           <option value="fullname">Full Name</option>
           <option value="details">Details</option>
           <option value="address">Address</option>
           <option value="phone">Phone</option>
         </select>
         </div>
         </div>
                <div class="form-group"><label class="col-sm-2 control-label" for="update">Update:  </label>
                <div class="col-sm-10"><input type="text" name="update" class="form-control">
                </div>
                </div>
                </div>
                <div class="box-footer">

              <div id="success" style="color:#34495e;"></div>
          <input type="submit" class="btn btn-primary" id="submit" name="SUBMIT" value="SUBMIT">
          </div>
       </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript" src="public/js/script.js"></script>

  <script type="text/javascript">
   

    $('#submit').click(function() {

      $.post("student_dashboard/update_student", $("#update-form").serialize(), function(response) {
        $('#success').html(response);
      });
      return false;
    });
  </script>