
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Classmates
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo URL?>student_dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Classmates</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Main row -->
      <div class="row">
      <div class="col-md-12">
              <!-- USERS LIST -->
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>

                  <div class="box-tools pull-right">
                    <span class="label label-danger"><?php echo $classmates ?> Classmate(s)</span>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                  <ul class="users-list clearfix">
      <?php foreach ($mate as $key => $value) { ?>
                    <li>
                      <img src="public/images/students/<?php echo $value['pix'] ; ?>" alt="User Image" style="height: 100px; width: 100px;">
                      <a class="users-list-name"><?php echo $value['fullname'] ; ?></a>
                      <span class="users-list-date"><a href = "<?php echo URL?>student_profile?student_id=<?php echo urlencode(base64_encode($value['student_id'])) ; ?>">View Profile</a></span>
                      <span class="users-list-date"><a href = "<?php echo URL?>compose_msg?student_id=<?php echo urlencode(base64_encode($value['student_id'])) ; ?>">Message</a></span>
                    </li>
                    
                    <?php } ?>
                  </ul>
                  <!-- /.users-list -->
                </div>
                <!-- /.box-body -->
              </div>
              <!--/.box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
          </section>
          </div>