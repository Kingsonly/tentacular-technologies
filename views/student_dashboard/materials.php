 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Material
        <small>PDF</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Widgets</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Small boxes (Stat box) -->
      <div class="row">
      <?php  foreach ($material as $key => $value) { ?>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h5><stron><b><?php echo $value['name'] ; ?></b></stron></h5>

              <p>Tutor Uploaded: <?php echo $value['admin_uploaded'] ; ?></p>
              <p>Date Uploaded: <?php echo $value['date_uploaded'] ; ?></p>
              <p>Material Type: <?php echo $value['type'] ; ?></p>
            </div>
            <div class="icon">
              <i class="fa fa-files-o"></i>
            </div>
            <a href="<?php echo URL; ?>materials?file=public/materials/<?php echo $value['file'] ; ?>" class="small-box-footer">
              Download <i class="fa fa-arrow-circle-down"></i>
            </a>
          </div>
        </div>

        <!-- ./col -->
        <?php } ?>
        </div>
        </section>
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      <!-- materials for video-->
      
       <section class="content-header">
      <h1>
        Material
        <small>Videos</small>
      </h1>
      
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Small boxes (Stat box) -->
      <div class="row">
      <?php foreach ($material2 as $key => $value) { ?>
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $value['name'] ; ?></h3>

              <p>Tutor Uploaded: <?php echo $value['admin_uploaded'] ; ?></p>
              <p>Date Uploaded: <?php echo $value['date_uploaded'] ; ?></p>
              <p>Material Type: <?php echo $value['type'] ; ?></p>
            </div>
            <div class="icon">
              <i class="fa fa-files-o"></i>
            </div>
            <a href="" class="small-box-footer">
              Download <i class="fa fa-arrow-circle-down"></i>
            </a>
          </div>
        </div>

        <!-- ./col -->
        <?php } ?>
        </div>
        </section>
        </div>