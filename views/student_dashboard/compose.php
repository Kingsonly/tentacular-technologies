
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Compose Message
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo URL?>index"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?php echo URL?>classmates"><i class="fa fa-users"></i> Classmates</a></li>
        <li class="active">Compose Message</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-md-3">
          <a href="<?php echo URL?>classmates" class="btn btn-primary btn-block margin-bottom">Back To Classmates</a><br>
</div>
        <!-- /.col -->
        <div class="col-md-12">
          <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
            <form method="post" enctype="multipart/form-data" action="compose_msg/sendmsg">
            
                <input type="hidden"  name="sender" value="<?php echo $fullname ?>" >
             
                <input type="hidden"  name="student_id" value="<?php echo base64_decode(urldecode($_GET['student_id'])); ?>">
           
              <div class="form-group">
                <input class="form-control" name="subject" placeholder="Subject">
              </div>
              <div class="form-group">
                    <textarea class="form-control" name="message" placeholder="Message" rows="7"></textarea>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <input type="submit" class="btn btn-primary" value ="SEND">
              </div>
              <input type="reset" class="btn btn-default" value = "CLEAR">
            </div>
              </form>
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->