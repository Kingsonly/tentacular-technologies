-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 22, 2017 at 09:23 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tent`
--

-- --------------------------------------------------------

--
-- Table structure for table `academy_pricing_plans`
--

CREATE TABLE `academy_pricing_plans` (
  `plan_id` int(255) NOT NULL,
  `plan` varchar(50) NOT NULL,
  `price` int(10) NOT NULL,
  `duration` int(10) NOT NULL,
  `packages` varchar(200) NOT NULL,
  `weekdays` varchar(20) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `academy_pricing_plans`
--

INSERT INTO `academy_pricing_plans` (`plan_id`, `plan`, `price`, `duration`, `packages`, `weekdays`, `status`) VALUES
(1, 'Starter', 10000, 1, 'Web Design-1, Web Development-1, Graphics Design-1', 'Saturdays', 2017),
(2, 'Basic', 25000, 2, 'Web Design-2, Web Development-2, Graphics Design-2', 'Thursdays & Fridays', 2017),
(3, 'Advanced', 50000, 3, 'Web Design-3, Web Development-3, Graphics Design-3, Online Marketing', 'Monday - Wednesday', 2017),
(4, 'professional', 3000, 1, 'Learn different things such as building a game,building a robot,building your own codeless website, etc', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `academy_projects`
--

CREATE TABLE `academy_projects` (
  `project_id` int(255) NOT NULL,
  `project_name` varchar(100) NOT NULL,
  `students` varchar(100) NOT NULL,
  `project_desc` varchar(1000) NOT NULL,
  `date` date NOT NULL,
  `status` varchar(100) NOT NULL,
  `skills` varchar(200) NOT NULL,
  `pix` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `academy_projects`
--

INSERT INTO `academy_projects` (`project_id`, `project_name`, `students`, `project_desc`, `date`, `status`, `skills`, `pix`) VALUES
(1, 'Project No. 1', 'Wesley Prior & Rita George', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sed facilisis purus. Donec interdum massa at ipsum vehicula tristique. Maecenas bibendum dictum tincidunt. Sed nec justo ac libero consequat tincidunt. Cras eget molestie justo', '2013-03-15', 'Finish on 3 Aug, 2013', 'web develpment, design', 'portfolio-img1.jpg'),
(2, 'Project No. 2', 'Rita George', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sed facilisis purus. Donec interdum massa at ipsum vehicula tristique. Maecenas bibendum dictum tincidunt. Sed nec justo ac libero consequat tincidunt. Cras eget molestie justo', '2013-05-15', 'Finish on 30 Dec, 2013', 'web develpment, design', 'portfolio-img2.jpg'),
(3, 'Project No. 3', 'Advanced Class January-March 2017', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sed facilisis purus. Donec interdum massa at ipsum vehicula tristique. Maecenas bibendum dictum tincidunt. Sed nec justo ac libero consequat tincidunt. Cras eget molestie justo', '2016-01-15', 'Finish on 30 Oct, 2016', 'web develpment, design', 'portfolio-img3.jpg'),
(4, 'Project No. 4', 'John Doe', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam sed facilisis purus. Donec interdum massa at ipsum vehicula tristique. Maecenas bibendum dictum tincidunt. Sed nec justo ac libero consequat tincidunt. Cras eget molestie justo', '2017-04-15', 'On-going', 'web develpment, design', 'portfolio-img4.jpg'),
(5, 'Project No. 5', 'Wesley Prior', 'Project5', '2017-02-03', 'On-going', 'CSS, Bootstrap', 'portfolio-img5.jpg'),
(6, 'Project No. 6', 'Class of 2016', 'A very large project', '2017-01-01', 'On-going', 'Web design, Mobile, Desktop Applications', 'portfolio-img6.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `academy_students`
--

CREATE TABLE `academy_students` (
  `student_id` int(255) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `paid_status` varchar(10) NOT NULL DEFAULT 'NONE',
  `paid` int(10) NOT NULL DEFAULT '0',
  `completed_course` varchar(10) NOT NULL DEFAULT 'NO',
  `academy_remark` varchar(100) NOT NULL DEFAULT 'NONE',
  `pix` varchar(50) NOT NULL DEFAULT 'nopix.jpg',
  `date` date NOT NULL,
  `datetime` datetime NOT NULL,
  `course` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `academy_students`
--

INSERT INTO `academy_students` (`student_id`, `username`, `password`, `fullname`, `address`, `email`, `phone`, `paid_status`, `paid`, `completed_course`, `academy_remark`, `pix`, `date`, `datetime`, `course`) VALUES
(1, 'ESE', 'ESE', 'Dahlia Akhaine', 'Minna', 'dahlia@gmail.com', '07051365997', 'NONE', 0, 'NO', 'NONE', 'nopix.png', '2017-05-01', '2017-05-01 02:07:47', 'Online Marketing'),
(5, 'KINGS', '2131b', 'Kingsley Achumie', 'Abuja', 'kingsley@yahoo.com', '08112934949', 'NONE', 0, 'NO', 'NONE', 'nopix.png', '2017-05-01', '2017-05-01 03:00:55', 'Web Design'),
(6, 'BORI', 'c608b', 'Bori Matthew', 'Dutse', 'bori@ymail.com', '0903838383', 'NONE', 0, 'NO', 'NONE', 'nopix.png', '2017-05-01', '2017-05-01 09:23:36', 'Online Marketing'),
(7, 'kingsonly', 'first', 'kingsley achumie', 'kugiugugh', 'kingsonly13c@gmail.com', '08153259099', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 07:08:46', ''),
(8, 'fiuhiuerf', 'rufhi', 'erferfe', 'ejhfuekhjfr', 'kingsonly13k@gmail.com', '34567u8iop', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 09:35:12', 'Web Design'),
(9, 'fiuhiuerf', 'rufhi', 'erferfe', 'ejhfuekhjfr', 'kingsonly13j@gmail.com', '34567u8iop', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 09:35:31', 'Web Design'),
(10, 'fiuhiuerf', 'rufhi', 'erferfe', 'ejhfuekhjfr', 'kingsonly13j@gmail.com', '34567u8iop', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 09:37:23', 'Web Design'),
(11, 'fiuhiuerf', 'rufhi', 'erferfe', 'ejhfuekhjfr', 'kingsonly13h@gmail.com', '34567u8iop', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 09:37:31', 'Web Design'),
(12, 'geger', 'rfefe', 'egege', 'etgetgg', 'egege@gmail.com', 'tegetetwge', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 09:41:36', 'Web Development'),
(13, 'geger', 'rfefe', 'egege', 'etgetgg', 'kingsonly13kca@gmail.com', 'tegetetwge', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 09:41:55', 'Web Development'),
(14, 'eojrfoierj', 'erjfe', 'uhfruhreifr ruifnueirf', 'eorufjoiefj', 'kkkkkabinas@gmail.com', 'ekrhfuejfh', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 09:52:28', 'Web Design'),
(15, 'eojrfoierj', 'erjfe', 'uhfruhreifr ruifnueirf', 'eorufjoiefj', 'boy13@gmail.com', 'ekrhfuejfh', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 09:52:53', 'Web Design'),
(16, 'eojrfoierj', 'erjfe', 'uhfruhreifr ruifnueirf', 'eorufjoiefj', 'boy135@gmail.com', 'ekrhfuejfh', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 09:53:57', 'Web Design'),
(17, 'eojrfoierj', 'erjfe', 'uhfruhreifr ruifnueirf', 'eorufjoiefj', 'boy13t@gmail.com', 'ekrhfuejfh', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 09:54:07', 'Web Design'),
(18, 'eojrfoierj', '', 'uhfruhreifr ruifnueirf', 'eorufjoiefj', 'boy13t@gmail.com', 'ekrhfuejfh', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 09:54:23', 'Web Design'),
(19, 'eojrfoierj', 'ererg', 'uhfruhreifr ruifnueirf', 'eorufjoiefj', 'boyandgirls13@gmail.com', 'ekrhfuejfh', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 09:54:41', 'Web Design'),
(20, 'rferferfer', 'erere', '3r43r3', 'errege', 'boyandgirls13@gmail.com', 'erer', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 09:55:06', ''),
(21, 'rferferfer', '', '3r43r3', 'errege', 'boyandgirlswith13@gmail.com', 'erer', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 10:09:32', ''),
(22, 'iruhgiruht', 'irg', 'tgihtiuhg thguihtg', 'jhgnthjbtg', 'joy@gmail.com', 'ihitjehbghtgn', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 10:11:46', 'Web Design'),
(23, 'fnfnfnfnnf', 'fffnf', 'ffnnf fnfnfn fnfnf', 'fjfjfjjfjf', 'kismewe@gmail.com', 'nfnfnfnfn', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 10:28:06', 'Web Design'),
(24, 'test', 'ihiuh', 'rgh rthjgt gkrjtgrtg ', 'tjhguirtghiutrg', 'emeak@gmail.com', 'ethgiuhtgirutg', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 10:32:50', 'Web Design'),
(25, 'eeeeee', 'etgeu', 'kejgnjekg etbg', 'outguthie', 'jkambad@gmail.com', 'euthgiuehg', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 10:37:33', 'Graphics Design'),
(26, 'yeshberf', 'iehgb', 'rtgrtg', 'ituhgiruthgrtg', 'comeon@gmail.com', 'ihgitrhgritg', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 10:39:42', ''),
(27, 'gtgjnbbbf ', 'jfjfj', 'grtgrtg', 'fjfjjfjjfjfjf', 'rtgrtg@gmail.com', 'ffjfjjfjjfjfj', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 10:47:27', 'Web Design'),
(28, 'eieieieiei', 'eeeke', 'erhuighieguh', 'kekekekek', 'heheheh@gmail.com', 'kkekekekek', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 10:50:19', 'Web Development'),
(29, 'final', 'djdjd', 'ahdajbd diuhuis', 'sksksksksksk', 'final@gmail.com', 'jjfjfjfj', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 11:42:45', 'Codeless Programming'),
(30, 'firsdt', 'djdjj', 'kbfsj dshdbf', 'dkdkdkdk', 'hdhdhdhh@gmail.com', 'djdjdjdjjd', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 11:44:13', 'Codeless Programming'),
(31, 'skhfjhkf', 'skhfh', 'tess', 'shfsjhf', 'test@gmail.com', 'shfsjkfhksjf', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 11:51:31', 'Codeless Programming'),
(32, 'kingsonly1', '4b364', 'ksdbfhsdbf', 'khkhbk', 'first@gmail.com', 'khkhbh', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 23:11:27', 'Codeless Programming'),
(33, 'testing1', '4b364', 'asdedad', 'hjk,.', 'adadasd@gmail.com', 'hjk.', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 23:23:43', 'Codeless Programming'),
(34, 'fin3', '4b3642f5eca84536f9233ec51e6a164a', 'achumie kingsley', 'hjrgjfne f', 'kingsonly13jabc@gmail.com', 'yuiopwebn', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-09', '2017-05-09 23:35:49', 'Codeless Programming'),
(35, 'qwertyu', '164ced40da0a61fa08cfed0412dc587f', 'asdfghjkl', 'rtyukl;', 'asdfg@gmail.com', 'fghjkl;', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-10', '2017-05-10 01:15:50', 'Codeless Programming'),
(36, 'qwertyuhhh', '164ced40da0a61fa08cfed0412dc587f', 'asdfghjkl', 'rtyukl;', 'asdfgjj@gmail.com', 'fghjkl;', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-10', '2017-05-10 01:29:23', 'Codeless Programming'),
(37, 'qwertyuhhh', '240fd7457e9cc0b7c82761fcdbfa51a8', 'asdfghjkl', 'rtyukkkl;', 'asdfgjggj@gmail.com', 'fghjjjkl;', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-10', '2017-05-10 01:30:07', 'Codeless Programming'),
(38, 'kboy', '4b3642f5eca84536f9233ec51e6a164a', 'achumie kboy', 'oiuyuihubhjjmk', 'kaboy@gmail.com', '000000000', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-10', '2017-05-10 02:13:12', 'Codeless Programming'),
(39, 'payment', 'f83c2a85d972a89238f31296c63f0dbc', 'payment', 'payment', 'payment@gmail.com', 'payment', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-10', '2017-05-10 07:45:50', ''),
(40, 'http://ten', '61305ca9c408b334d1d36cacbbdd237b', 'http://tentacularltd.com/codeless', 'http://tentacularltd.com/codeless', 'aaaa@gmail.com', 'http://tentacularltd', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-10', '2017-05-10 07:51:02', 'Codeless Programming'),
(41, 'http://ten', '61305ca9c408b334d1d36cacbbdd237b', 'http://tentacularltd.com/codeless http://tentacularltd.com/codeless', 'http://tentacularltd.com/codeless', 'fjjjfjfjfj@gmail.com', 'http://tentacularltd', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-10', '2017-05-10 07:54:21', 'Codeless Programming'),
(42, 'tentacular', '0a3f057b1f8b148b79cd3f8f70c661b4', 'Achumie Kingsley', 'ncce quaters mabushi abuja ', 'info1@tentacularltd.com', '08153259099', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-10', '2017-05-10 12:56:28', 'Codeless Programming'),
(43, '4r3r', 'etgtg', 'r4rerr', 'etgetgetgetg', '34r3r3t434r@gmm.fff', 'tggetgetgetgeg', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-21', '2017-05-21 19:50:38', 'Web Development'),
(44, '4r3r', '3t3t3', 'r4rerr', 'etgetgetgetg', '34r3r3t434r@gmm.fff', 'tggetgetgetgeg', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-21', '2017-05-21 19:51:33', 'Web Development'),
(45, 'uhuhuihi', 'iuhujhn', 'iuhuhu', 'iuhuihuihiuhiu', 'ijoijoijoijoi@ggg.com', 'ojhkkhik', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-21', '2017-05-21 19:52:22', 'Web Development'),
(46, 'rthrth', 'tgtgrgr', 'rgtetget', 'rhrthrh', 'rgtrtgrh@gmail.com', 'rthrhrh', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-21', '2017-05-21 20:13:49', 'Web Development'),
(47, 'rthrth', 'tgtgrgr', 'rgtetget', 'rhrthrh', 'rgtrtgrh@gmail.com', 'rthrhrh', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-21', '2017-05-21 20:19:24', 'Web Development'),
(48, 'rthrth', 'tgtrgrth', 'rgtetget', 'rhrthrh', 'rgtrtgrh@gmail.com', 'rthrhrh', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-21', '2017-05-21 20:20:16', 'Web Development'),
(49, 'egreg', 'efgeg', 'rfegeg', 'trgrsg', 'egege@gmail.com', 'trgrtgr', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-21', '2017-05-21 20:35:25', ''),
(50, 'rferferg', 'egeg', 'rfwww', 'egegeg', 'rrfre@gmm.ccc', 'etg', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-21', '2017-05-21 20:36:54', 'Web Development'),
(51, 'rferferg', 'eyheyh', 'rfwww', 'egegeg', 'rrfre@gmm.cccyy', 'etg', 'NONE', 0, 'NO', 'NONE', 'nopix.jpg', '2017-05-21', '2017-05-21 20:43:47', 'Web Development');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(255) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(5) NOT NULL,
  `details` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `pix` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `fullname`, `username`, `password`, `details`, `email`, `phone`, `pix`) VALUES
(1, 'Ese Joy', 'ESE', 'ESE', 'Details', 'ese@gmail.com', '0908766', '');

-- --------------------------------------------------------

--
-- Table structure for table `admin_msg`
--

CREATE TABLE `admin_msg` (
  `msg_id` int(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subject` varchar(1000) NOT NULL,
  `message` varchar(1000) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'unread',
  `date` date NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_msg`
--

INSERT INTO `admin_msg` (`msg_id`, `name`, `email`, `subject`, `message`, `status`, `date`, `datetime`) VALUES
(1, 'John Doe', 'johndoe@ymail.com', 'A very interesting topic', 'This is the body of my message. Though quite short.', 'unread', '2017-04-16', '2017-04-16 19:44:51'),
(2, 'myname', 'johndoe@ymail.com', 'A very interesting topic', 'This is the body of my message. Though quite short.', 'unread', '2017-04-16', '2017-04-16 20:26:39'),
(3, 'myname', 'johndoe@ymail.com', 'A very interesting topic', 'This is the body of my message. Though quite short.', 'read', '2017-04-16', '2017-04-16 20:27:36');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `client_id` int(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `details` varchar(1000) NOT NULL,
  `pix` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`client_id`, `name`, `details`, `pix`) VALUES
(1, 'adloyalty business network', 'Client1 details', 'adloyalty.png'),
(2, 'Lifebuilders initiative', 'Client2 details', 'lifebuilders.png'),
(3, 'winners properties', 'Client3 details', 'winners.png'),
(4, 'unizik law faculty', 'Client4 details', 'unizik_logo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `mailing_list`
--

CREATE TABLE `mailing_list` (
  `mail_id` int(255) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mailing_list`
--

INSERT INTO `mailing_list` (`mail_id`, `email`) VALUES
(1, 'esejoyakhaine@gmail.com'),
(2, 'esejoy@gmail.com'),
(3, 'someone@yandex.com'),
(4, 'someone@yandex.co.uk'),
(5, 'dahlia@gmail.com'),
(6, 'kingsley@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `newsfeed`
--

CREATE TABLE `newsfeed` (
  `news_id` int(255) NOT NULL,
  `title` varchar(200) NOT NULL,
  `details` varchar(1500) NOT NULL,
  `date` date NOT NULL,
  `short_date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newsfeed`
--

INSERT INTO `newsfeed` (`news_id`, `title`, `details`, `date`, `short_date`) VALUES
(1, 'newsfeed01', 'details on neewsfeed01', '2017-01-17', '17 jan'),
(2, 'newsfeed02', 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat Lorem pariatur', '2016-07-01', '01 jul'),
(3, 'newsfeed03', 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat Lorem pariatur', '2017-04-16', '16 apr'),
(4, 'newsfeed04', 'details about newsfeed4', '2017-03-27', '27 mar'),
(5, 'newsfeed05', 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat Lorem pariatur', '2017-03-30', '30 mar');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `partner_id` int(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `pix` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`partner_id`, `name`, `pix`) VALUES
(1, 'partner1', 'partner-1.png'),
(2, 'partner2', 'partner-1.png'),
(3, 'partner3', 'partner-1.png'),
(4, 'partner4', 'partner-1.png'),
(5, 'partner5', 'partner-1.png'),
(6, 'partner6', 'partner-1.png'),
(7, 'partner7', 'partner-1.png');

-- --------------------------------------------------------

--
-- Table structure for table `portfolio`
--

CREATE TABLE `portfolio` (
  `portfolio_id` int(255) NOT NULL,
  `job_name` varchar(100) NOT NULL,
  `client` varchar(100) NOT NULL,
  `job_desc` varchar(1000) NOT NULL,
  `date` year(4) NOT NULL,
  `status` varchar(100) NOT NULL,
  `skills` varchar(200) NOT NULL,
  `pix` varchar(50) NOT NULL,
  `pix_folder` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portfolio`
--

INSERT INTO `portfolio` (`portfolio_id`, `job_name`, `client`, `job_desc`, `date`, `status`, `skills`, `pix`, `pix_folder`) VALUES
(1, 'Nnamdi Azikiwe University Portal (Faculty of Law)', 'Nnamdi Azikiwe University', 'Development of Student portal for the Faculty of Law, Nnamdi Azikiwe University', 2013, 'Completed', 'web develpment, design', 'unizik_logo.jpg', 'general'),
(2, 'Adloyalty Business Network Official Website', 'Adloyalty Business Network', 'Development of the Oficial website for Adloyalty Business Network', 2017, 'On-going', 'web develpment, design', 'Adloyalty.png', 'general'),
(3, 'Winners Properties Official Website', 'Winners Properties Limited', 'Development of the Oficial website for Winners Properties', 2017, 'Completed', 'web develpment, design', 'winners.png', 'general'),
(4, 'Life Builders Initiative Official Website', 'Life Builders Initiative', 'Development of the Oficial website for Life Builders Initiative', 2015, 'Completed', 'web develpment, design', 'lifebuilders.png', 'general');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(255) NOT NULL,
  `title` varchar(200) NOT NULL,
  `details` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `title`, `details`) VALUES
(1, 'Web Application Development', 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat Lorem pariatur'),
(2, 'Database Management', 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat Lorem pariatur'),
(3, 'Website Design', 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat Lorem pariatur'),
(4, 'Mobile Application Development', 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat Lorem pariatur'),
(5, 'Practical Training on Professional Courses', 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat Lorem pariatur'),
(6, 'Graphics Design and Animation', 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat Lorem pariatur'),
(7, 'Internet of Things', 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat Lorem pariatur'),
(8, 'Private & Public Mentoring/ Tutoring', 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat Lorem pariatur'),
(9, 'Search Engine Optimization', ''),
(10, 'Web Marketing', '');

-- --------------------------------------------------------

--
-- Table structure for table `student_academy_details`
--

CREATE TABLE `student_academy_details` (
  `academy_id` int(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `fullname` varchar(244) NOT NULL,
  `student_id` int(11) NOT NULL,
  `plan` varchar(255) NOT NULL,
  `course` varchar(50) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_academy_details`
--

INSERT INTO `student_academy_details` (`academy_id`, `username`, `fullname`, `student_id`, `plan`, `course`, `status`) VALUES
(1, 'ESE', '', 1, 'Basic', 'Online Marketing', 1),
(5, 'KINGS', '', 0, 'Advanced', 'Web Design', 0),
(6, 'BORI', '', 0, 'Advanced', 'Online Marketing', 0),
(7, 'kingsonly', '', 0, 'Starter', '', 0),
(8, 'fiuhiuerf', '', 0, 'profession', 'Web Design', 0),
(9, 'fiuhiuerf', '', 0, 'profession', 'Web Design', 0),
(10, 'fiuhiuerf', '', 0, 'profession', 'Web Design', 0),
(11, 'fiuhiuerf', '', 0, 'profession', 'Web Design', 0),
(12, 'geger', '', 0, 'profession', 'Web Development', 0),
(13, 'geger', '', 0, 'profession', 'Web Development', 0),
(14, 'eojrfoierjf', '', 0, 'profession', 'Web Design', 0),
(15, 'eojrfoierjf', '', 0, 'profession', 'Web Design', 0),
(16, 'eojrfoierjf', '', 0, 'profession', 'Web Design', 0),
(17, 'eojrfoierjf', '', 0, 'profession', 'Web Design', 0),
(18, 'eojrfoierjf', '', 0, 'profession', 'Web Design', 0),
(19, 'eojrfoierjf', '', 0, 'profession', 'Web Design', 0),
(20, 'rferferferf', '', 0, 'profession', '', 0),
(21, 'rferferferf', '', 0, 'profession', '', 0),
(22, 'iruhgiruhtg rituhiruthgbt girtuhg', '', 0, 'profession', 'Web Design', 0),
(23, 'fnfnfnfnnf', '', 0, 'profession', 'Web Design', 0),
(24, 'test', '', 0, 'profession', 'Web Design', 0),
(25, 'eeeeee', '', 0, 'profession', 'Graphics Design', 0),
(26, 'yeshberf', '', 0, 'professional', '', 0),
(27, '1', '', 4, '2', '3', 0),
(28, 'eieieieiei', '', 28, 'profession', 'Web Development', 0),
(29, 'final', '', 29, 'profession', 'Codeless Programming', 0),
(30, 'firsdt', '', 30, 'profession', 'Codeless Programming', 0),
(31, 'skhfjhkf', '', 31, 'profession', 'Codeless Programming', 0),
(32, 'kingsonly123', '', 32, 'profession', 'Codeless Programming', 0),
(33, 'testing1', '', 33, 'profession', 'Codeless Programming', 0),
(34, 'fin3', '', 34, 'profession', 'Codeless Programming', 0),
(35, 'qwertyu', '', 35, 'profession', 'Codeless Programming', 0),
(36, 'qwertyuhhhh', '', 36, 'profession', 'Codeless Programming', 0),
(37, 'qwertyuhhhhjjj', '', 37, 'profession', 'Codeless Programming', 0),
(38, 'kboy', '', 38, 'professional', 'Codeless Programming', 0),
(39, 'payment', '', 39, 'professional', '', 0),
(40, 'http://tentacularltd.com/codeless', '', 40, 'professional', 'Codeless Programming', 0),
(41, 'http://tentacularltd.com/codelesshttp://tentacularltd.com/codeless', '', 41, 'professional', 'Codeless Programming', 0),
(42, 'tentacular', '', 42, 'professional', 'Codeless Programming', 0),
(43, 'rthrth', 'rgtetget', 0, 'Starter', 'Web Development', 0),
(44, 'rthrth', 'rgtetget', 0, 'Starter', 'Web Development', 0),
(45, 'rthrth', 'rgtetget', 0, 'Starter', 'Web Development', 0),
(46, 'egreg', 'rfegeg', 0, 'Starter', '', 0),
(47, 'rferferg', 'rfwww', 50, 'Starter', 'Web Development', 0),
(48, 'rferferg', 'rfwww', 51, 'Starter', 'Web Development', 0);

-- --------------------------------------------------------

--
-- Table structure for table `student_materials`
--

CREATE TABLE `student_materials` (
  `material_id` int(255) NOT NULL,
  `name` varchar(200) NOT NULL,
  `type` varchar(50) NOT NULL,
  `plan` varchar(20) NOT NULL,
  `course` varchar(255) NOT NULL,
  `date_uploaded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `admin_uploaded` varchar(10) NOT NULL,
  `file` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_materials`
--

INSERT INTO `student_materials` (`material_id`, `name`, `type`, `plan`, `course`, `date_uploaded`, `admin_uploaded`, `file`) VALUES
(1, 'Material1', 'pdf', 'Basic', 'Online Marketing', '2017-04-27 13:12:33', 'ESE', 'BeginningHTML5andCSS3ForDummies.pdf'),
(2, 'Material2', 'pdf', 'Basic', '', '2017-04-27 13:13:17', 'ESE', 'Maxwell-360DegreeLeader.pdf'),
(3, 'Material1', 'pdf', 'Advanced', 'Online Marketing', '2017-04-27 13:17:35', 'ESE', 'forstyle.pdf'),
(4, 'Material3', 'pdf', 'Basic', '', '2017-04-27 13:23:03', 'ESE', '944875912_AngularJS_Tutorial_W3Schools.pdf'),
(5, 'Create shop codlessly', 'pdf', 'professional', 'Codeless Programming', '2017-05-10 00:11:38', 'kingsonly', 'Create-e-commerce-website-Opencart.pdf'),
(6, 'getting started with WordPress', 'pdf', 'professional', 'Codeless Programming', '2017-05-10 00:11:38', 'kingsonly', 'getting-started-with-WordPress-ebook.pdf'),
(7, 'wordpress tutorial', 'pdf', 'professional', 'Codeless Programming', '2017-05-10 00:11:38', 'kingsonly', 'wordpress_tutorial.pdf'),
(8, 'html5 and css3 for dummies', 'pdf', 'professional', 'Codeless Programming', '2017-05-10 00:11:38', 'kingsonly', 'html5-and-css3-all-in-one-for-dummies-3rd-edition-andy-harris.pdf'),
(9, '', 'pdf', 'professional', 'Codeless Programming', '2017-05-10 00:11:38', 'kingsonly', 'WixTutorial.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `student_msg`
--

CREATE TABLE `student_msg` (
  `msg_id` int(255) NOT NULL,
  `sender` varchar(500) NOT NULL,
  `receiver` varchar(500) NOT NULL,
  `subject` varchar(500) NOT NULL,
  `message` varchar(10000) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'unread',
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_msg`
--

INSERT INTO `student_msg` (`msg_id`, `sender`, `receiver`, `subject`, `message`, `status`, `datetime`) VALUES
(1, 'Dahlia Akhaine', 'Bori Matthew', 'Test message', 'Hey whatsup', 'read', '2017-05-01 09:09:43'),
(2, 'Bori Matthew', 'Dahlia Akhaine', 'Reply', 'm good, u?', 'read', '2017-05-01 09:55:18'),
(3, 'Dahlia Akhaine', 'Bori Matthew', 'New reply', 'still testing the system', 'read', '2017-05-04 17:47:22'),
(4, 'Bori Matthew', 'kboy', 'New reply', 'ok, i think were making progress', 'read', '2017-05-04 17:54:35');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `team_id` int(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `position` varchar(100) NOT NULL,
  `skills` varchar(1000) NOT NULL,
  `pix` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`team_id`, `name`, `position`, `skills`, `pix`) VALUES
(1, 'Kingsley Achumie', 'CEO | Founder', 'Application Development | Internet Of Things | Mobile', 'kingsley.jpg'),
(2, 'Michael Akpobome', 'Co-Founder', 'Application Development | Mobile | SEO', 'mike.jpg'),
(3, 'Dahlia Akhaine', 'Lead Developer', 'Web Design | Database Management | Application Development', 'dahlia.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `term_id` int(255) NOT NULL,
  `term` varchar(100) NOT NULL,
  `details` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `testimony_id` int(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `testimony` varchar(1500) NOT NULL,
  `details` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`testimony_id`, `name`, `testimony`, `details`) VALUES
(1, 'Testifier01', 'Implicit details about your experience with tentacular', 'Customer'),
(2, 'Testifier02', 'Implicit details about your experience with tentacular', 'Customer');

-- --------------------------------------------------------

--
-- Table structure for table `values`
--

CREATE TABLE `values` (
  `value_id` int(255) NOT NULL,
  `value` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `weekly_challenge`
--

CREATE TABLE `weekly_challenge` (
  `challenge_id` int(255) NOT NULL,
  `name` varchar(5000) NOT NULL,
  `details` varchar(500) NOT NULL,
  `date` date NOT NULL,
  `plan` varchar(20) NOT NULL,
  `course` varchar(50) NOT NULL,
  `winner` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'OFF',
  `no_of_students` int(20) NOT NULL DEFAULT '0',
  `deadline` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weekly_challenge`
--

INSERT INTO `weekly_challenge` (`challenge_id`, `name`, `details`, `date`, `plan`, `course`, `winner`, `status`, `no_of_students`, `deadline`) VALUES
(1, 'Challenge01', 'Details on your first challenge', '2017-05-11', 'Advanced', 'Online Marketing', '', 'ON', 0, '2017-05-13 23:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `weekly_challenge_answers`
--

CREATE TABLE `weekly_challenge_answers` (
  `answer_id` int(255) NOT NULL,
  `challenge_id` int(255) NOT NULL,
  `challenge_name` varchar(5000) NOT NULL,
  `challenge_date` date NOT NULL,
  `answer` varchar(5000) NOT NULL,
  `answer_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `student_name` varchar(100) NOT NULL,
  `ratings` int(255) NOT NULL DEFAULT '0',
  `admin_ratings` int(10) NOT NULL DEFAULT '0',
  `status` varchar(10) NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weekly_challenge_answers`
--

INSERT INTO `weekly_challenge_answers` (`answer_id`, `challenge_id`, `challenge_name`, `challenge_date`, `answer`, `answer_date`, `student_name`, `ratings`, `admin_ratings`, `status`) VALUES
(1, 1, 'Challenge01', '2017-05-11', 'ANSWER', '2017-05-12 19:02:37', 'Dahlia Akhaine', 0, 0, 'ON'),
(2, 1, 'Challenge01', '2017-05-11', 'boris answer', '2017-05-13 04:54:17', 'Bori Matthew', 0, 0, 'ON'),
(3, 1, 'Challenge01', '2017-05-11', 'jjjhjnjbnjnjnjnj', '2017-05-19 19:34:57', 'achumie kboy', 0, 0, 'ON'),
(4, 1, 'Challenge01', '2017-05-11', 'JHHJHB', '2017-05-19 19:56:27', 'achumie kingsley', 0, 0, 'ON'),
(5, 1, 'Challenge01', '2017-05-11', 'HIBH', '2017-05-19 19:58:12', 'achumie kingsley', 0, 0, 'ON');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academy_pricing_plans`
--
ALTER TABLE `academy_pricing_plans`
  ADD PRIMARY KEY (`plan_id`);

--
-- Indexes for table `academy_projects`
--
ALTER TABLE `academy_projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `academy_students`
--
ALTER TABLE `academy_students`
  ADD PRIMARY KEY (`student_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admin_msg`
--
ALTER TABLE `admin_msg`
  ADD PRIMARY KEY (`msg_id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `mailing_list`
--
ALTER TABLE `mailing_list`
  ADD PRIMARY KEY (`mail_id`);

--
-- Indexes for table `newsfeed`
--
ALTER TABLE `newsfeed`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`partner_id`);

--
-- Indexes for table `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`portfolio_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `student_academy_details`
--
ALTER TABLE `student_academy_details`
  ADD PRIMARY KEY (`academy_id`);

--
-- Indexes for table `student_materials`
--
ALTER TABLE `student_materials`
  ADD PRIMARY KEY (`material_id`);

--
-- Indexes for table `student_msg`
--
ALTER TABLE `student_msg`
  ADD PRIMARY KEY (`msg_id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`team_id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`term_id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`testimony_id`);

--
-- Indexes for table `values`
--
ALTER TABLE `values`
  ADD PRIMARY KEY (`value_id`);

--
-- Indexes for table `weekly_challenge`
--
ALTER TABLE `weekly_challenge`
  ADD PRIMARY KEY (`challenge_id`);

--
-- Indexes for table `weekly_challenge_answers`
--
ALTER TABLE `weekly_challenge_answers`
  ADD PRIMARY KEY (`answer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academy_pricing_plans`
--
ALTER TABLE `academy_pricing_plans`
  MODIFY `plan_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `academy_projects`
--
ALTER TABLE `academy_projects`
  MODIFY `project_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `academy_students`
--
ALTER TABLE `academy_students`
  MODIFY `student_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admin_msg`
--
ALTER TABLE `admin_msg`
  MODIFY `msg_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mailing_list`
--
ALTER TABLE `mailing_list`
  MODIFY `mail_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `newsfeed`
--
ALTER TABLE `newsfeed`
  MODIFY `news_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `partner_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `portfolio_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `student_academy_details`
--
ALTER TABLE `student_academy_details`
  MODIFY `academy_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `student_materials`
--
ALTER TABLE `student_materials`
  MODIFY `material_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `student_msg`
--
ALTER TABLE `student_msg`
  MODIFY `msg_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `team_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `term_id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `testimony_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `values`
--
ALTER TABLE `values`
  MODIFY `value_id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `weekly_challenge`
--
ALTER TABLE `weekly_challenge`
  MODIFY `challenge_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `weekly_challenge_answers`
--
ALTER TABLE `weekly_challenge_answers`
  MODIFY `answer_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
