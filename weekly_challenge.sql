-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2017 at 01:12 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tent`
--

-- --------------------------------------------------------

--
-- Table structure for table `weekly_challenge`
--

CREATE TABLE IF NOT EXISTS `weekly_challenge` (
  `challenge_id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(5000) NOT NULL,
  `details` varchar(500) NOT NULL,
  `date` date NOT NULL,
  `plan` varchar(20) NOT NULL,
  `course` varchar(50) NOT NULL,
  `winner` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'OFF',
  `no_of_students` int(20) NOT NULL DEFAULT '0',
  `deadline` datetime NOT NULL,
  PRIMARY KEY (`challenge_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `weekly_challenge`
--

INSERT INTO `weekly_challenge` (`challenge_id`, `name`, `details`, `date`, `plan`, `course`, `winner`, `status`, `no_of_students`, `deadline`) VALUES
(1, 'Challenge01', 'Details on your first challenge', '2017-05-11', 'Advanced', 'Online Marketing', '', 'ON', 0, '2017-05-13 23:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
