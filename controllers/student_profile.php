<?php

class student_profile extends Controller {

	function __construct() {
		parent::__construct();
                Session::init();
                $logged = Session::get('loggedIn');
        
                $this->view->data['address']=Session::get('address');
                $this->view->data['username']=Session::get('username');
                $this->view->data['fullname']=Session::get('fullname');
                $this->view->data['email']=Session::get('email');
                $this->view->data['phone']=Session::get('phone');
                $this->view->data['paid_status']=Session::get('paid_status');
                $this->view->data['paid']=Session::get('paid');
                $this->view->data['completed_course']=Session::get('completed_course');
                $this->view->data['academy_remark']=Session::get('academy_remark');
                $this->view->data['student_id']=Session::get('student_id');
                $this->view->data['pix']=Session::get('pix');
                $this->view->data['unread']=Session::get('unread');
                $this->view->data['plan']=Session::get('plan');
                $this->view->data['course']=Session::get('course');
                $this->view->data['classmates']=Session::get('classmates');
                $this->view->data['msg_id']=Session::get('msg_id'); 
                $this->view->data['sender']=Session::get('sender');
                if ($logged == false) {
                        header('location: student_login');
                        exit;
                }
	}
	
	function index() {
                if(isset($_GET['student_id'])){
                        $student_id = base64_decode(urldecode($_GET['student_id']));
                        $course = $_SESSION['course'];
        $this->view->data['profile']=$this->model->get_classmate_profile($student_id);
        $this->view->data['academy']=$this->model->get_classmate_academy_details($student_id);
        $this->view->data['timeline']=$this->model->get_answers_timeline($student_id);
        $this->view->render('student_dashboard/profile',$noinclude=false,5);
        }
	}
}