<?php

class weekly_challenge extends Controller {

	function __construct() {
		parent::__construct();
                Session::init();
                $logged = Session::get('loggedIn');
        
                $this->view->data['address']=Session::get('address');
                $this->view->data['username']=Session::get('username');
                $this->view->data['fullname']=Session::get('fullname');
                $this->view->data['email']=Session::get('email');
                $this->view->data['phone']=Session::get('phone');
                $this->view->data['paid_status']=Session::get('paid_status');
                $this->view->data['paid']=Session::get('paid');
                $this->view->data['completed_course']=Session::get('completed_course');
                $this->view->data['academy_remark']=Session::get('academy_remark');
                $this->view->data['student_id']=Session::get('student_id');
                $this->view->data['pix']=Session::get('pix');
                $this->view->data['unread']=Session::get('unread');
                $this->view->data['plan']=Session::get('plan');
                $this->view->data['course']=Session::get('course');
                $this->view->data['classmates']=Session::get('classmates');
                $this->view->data['msg_id']=Session::get('msg_id'); 
                $this->view->data['sender']=Session::get('sender');
                if ($logged == false) {
                        header('location: student_login');
                        exit;
                }
	}
	
	function index() {
        $this->view->data['total_answers']=$this->model->get_total_weekly_challenge_answers();
        $this->view->data['answer']=$this->model->get_weekly_challenge_answers();
        $this->view->data['challenge']=$this->model->get_weekly_challenge_details();
        $this->view->render('student_dashboard/weekly_challenge',$noinclude=false,5);
	}

        function submit_answer()
        {
                if (isset($_POST['student_id'])) {
                        $student_name = $_POST['student_name'];
                        $student_id = $_POST['student_id'];
                        $answer = $_POST['answer'];
                        $challenge_id = $_POST['challenge_id'];
                        $challenge_name = $_POST['challenge_name'];
                        $challenge_date = $_POST['challenge_date'];
                        $status = "ON";
                        $check=$this->model->check_existing_answer($student_id,$answer);
                if($check>0){
                        echo "You have already sent this answer!"; // success message
                        }else{
                        $this->model->add_answer($student_name,$student_id,$answer,$challenge_id,$challenge_name,$challenge_date,$status);
                        header('location: ../weekly_challenge');
                        }
                }
        }
}
?>