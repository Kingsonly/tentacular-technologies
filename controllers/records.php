<?php 

class records extends Controller{

	function __construct() {

		parent::__construct();
		Session::init();
		$logged = Session::get('loggedIn');
        
		$this->view->data['details']=Session::get('details');
		$this->view->data['loggedUser']=Session::get('loggedUser');
		$this->view->data['fullname']=Session::get('fullname');
		$this->view->data['email']=Session::get('email');
		$this->view->data['phone']=Session::get('phone');

		$loggedid = Session::get('user_id');
 
		if ($logged == false) {
			Session::destroy();
			//header('location: ./login');
			exit;
		}
		
	}
	function index()
	{		
        $this->view->data['ad']=$this->model->get_admin();
        $this->view->data['stu']=$this->model->get_student_names();
        $this->view->data['par']=$this->model->get_partner();
        $this->view->data['cli']=$this->model->get_clients();
        $this->view->data['test']=$this->model->get_testimonials();
        $this->view->data['ser']=$this->model->get_service();
        $this->view->data['ter']=$this->model->get_terms();
		$this->view->render('records/index', $noinclude=false, 4);
	}

	function add_new_admin()
	{
        $username=$_POST['username'];
        $check=$this->model->check_existing_admin($username);
        if($check>0){
			Session::set('message_1', "Multiple records are not allowed");
		header("location: ../admin_dashboard");
        }else{
		if(isset($_POST['username'])){
			$username=$_POST['username'];
			$password=$_POST['password'];
			$fullname=$_POST['fullname'];
			$details=$_POST['details'];	
			$email=$_POST['email'];
			$phone=$_POST['phone'];	
			$this->model->add_admin($username,$password,$fullname,$details,$email,$phone);
			Session::set('message_1', "Record Successfully Added");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
        }
	}

	function update_existing_admin()
	{
		if(isset($_POST['username'])){

			$username=$_POST['username'];
			$update_field=$_POST['update_field'];
			$update=$_POST['update'];
			$this->model->update_admin($update_field,$update,$username);
			Session::set('message_1', "Record Successfully Updated");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
	}

	function delete_existing_admin()
	{
		if(isset($_POST['username'])){

			$username=$_POST['username'];
			$this->model->delete_admin($username);
			Session::set('message_1', "Record Successfully Deleted");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
	}

	function add_new_student()
	{
        $username=$_POST['username'];
        $check=$this->model->check_existing_student($username);
        if($check>0){
			Session::set('message_1', "Multiple records are not allowed");
		header("location: ../admin_dashboard");
        }else{
		if(isset($_POST['username'])){
			$username=$_POST['username'];
			$password=$_POST['password'];			
			$feature=$_POST['feature'] ;
			$fullname=$_POST['fullname'];
			$plan=$_POST['plan'];
			$address=$_POST['address'];
			$email=$_POST['email'];			
			$phone=$_POST['phone'] ;
			$paid_status=$_POST['paid_status'];
			$paid=$_POST['paid'];
			$completed_course=$_POST['completed_course'];
			$academy_remark=$_POST['academy_remark'];	
			$date = date("Y-m-d");
			$datetime = date("Y-m-d H:i:s");
			$this->model->add_student($username,$password,$fullname,$plan,$address,$email,$phone,$paid_status,$paid,$completed_course,$academy_remark,$date,$datetime);

			Session::set('message_1', "Record Successfully Added");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
        }
	}
		
	function update_existing_student()
	{
		if(isset($_POST['username'])){
			$username=$_POST['username'];
			$update_field=$_POST['update_field'];
			$update=$_POST['update'];
			$this->model->update_student($update_field,$update,$username);
			Session::set('message_1', "Record Successfully Updated");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
	}
	function delete_existing_student()
	{
		if(isset($_POST['username'])){
			$username=$_POST['username'];
			$this->model->delete_student($username);
			Session::set('message_1', "Record Successfully Deleted");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
	}

	function add_new_client()
	{
        $name=$_POST['name'];
        $check=$this->model->check_existing_client($name);
        if($check>0){
			Session::set('message_1', "Multiple records are not allowed");
		header("location: ../admin_dashboard");
        }else{
		if(isset($_POST['name'])){
			$name=$_POST['name'];
			$about=$_POST['details'];
			$phone=$_POST['contact'];	
			$this->model->add_client($name,$details,$contact);
			Session::set('message_1', "Record Successfully Added");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
        }
	}

	function update_existing_client()
	{
		if(isset($_POST['name'])){

			$name=$_POST['name'];
			$update_field=$_POST['update_field'];
			$update=$_POST['update'];
			$this->model->update_client($update_field,$update,$name);
			Session::set('message_1', "Record Successfully Updated");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
	}

	function delete_existing_client()
	{
		if(isset($_POST['name'])){

			$name=$_POST['name'];
			$this->model->delete_client($name);
			Session::set('message_1', "Record Successfully Deleted");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
	}

	function add_new_partner()
	{
        $name=$_POST['name'];
        $check=$this->model->check_existing_partner($name);
        if($check>0){
			Session::set('message_1', "Multiple records are not allowed");
		header("location: ../admin_dashboard");
        }else{
		if(isset($_POST['name'])){
			$name=$_POST['name'];		
			$this->model->add_partner($name,$pix);
			Session::set('message_1', "Record Successfully Added");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
        }
	}

	function delete_existing_partner()
	{
		if(isset($_POST['name'])){
			$name=$_POST['name'];
			$this->model->delete_partner($name);
			Session::set('message_1', "Record Successfully Deleted");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
	}

	function add_new_testimonial()
	{
        $name=$_POST['name'];
        $check=$this->model->check_existing_testimonial($name);
        if($check>0){
			Session::set('message_1', "Multiple records are not allowed");
		header("location: ../admin_dashboard");
        }else{
		if(isset($_POST['name'])){
			$name=$_POST['name'];
			$address=$_POST['address'];			
			$comment=$_POST['comment'];		

			$this->model->add_testimonial($name,$address,$comment);
			Session::set('message_1', "Record Successfully Added");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
        }
	}
	function delete_existing_testimonial()
	{
		if(isset($_POST['name'])){
			$name=$_POST['name'];
			$this->model->delete_testimonial($name);
			Session::set('message_1', "Record Successfully Deleted");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
	}

	function add_new_service()
	{
        $title=$_POST['title'];
        $check=$this->model->check_existing_service($title);
        if($check>0){
			Session::set('message_1', "Multiple records are not allowed");
		header("location: ../admin_dashboard");
        }else{
		if(isset($_POST['title'])){
			$title=$_POST['title'];
			$service=$_POST['service'];			
			$this->model->add_service($title,$service);
			Session::set('message_1', "Record Successfully Added");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
        }
	}
	function delete_existing_service()
	{
		if(isset($_POST['title'])){
			$title=$_POST['title'];
			$this->model->delete_service($title);
			Session::set('message_1', "Record Successfully Deleted");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
	}
	function add_new_term()
	{
        $term=$_POST['term'];
        $check=$this->model->check_existing_term($term);
        if($check>0){
			Session::set('message_1', "Multiple records are not allowed");
		header("location: ../admin_dashboard");
        }else{
		if(isset($_POST['term'])){
			$term=$_POST['term'];
			$details=$_POST['details'];	
			$this->model->add_term($term,$details);
			Session::set('message_1', "Record Successfully Added");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
        }
	}

	function delete_existing_term()
	{
		if(isset($_POST['term'])){

			$term=$_POST['term'];
			$this->model->delete_term($term);
			Session::set('message_1', "Record Successfully Deleted");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
	}
}