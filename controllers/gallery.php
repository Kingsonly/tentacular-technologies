<?php

class gallery extends Controller {

	function __construct() {

		parent::__construct();
		Session::init();
		$logged = Session::get('loggedIn');
        
		$this->view->data['details']=Session::get('details');
		$this->view->data['loggedUser']=Session::get('loggedUser');
		$this->view->data['fullname']=Session::get('fullname');
		$this->view->data['email']=Session::get('email');
		$this->view->data['phone']=Session::get('phone');
		$this->view->data['message_1']=Session::get('message_1');
		$this->view->data['admin_id']=Session::get('admin_id');

		$loggedid = Session::get('user_id');
 
		if ($logged == false) {
			Session::destroy();
			//header('location: ./login');
			exit;
		}
		
	}
	
	function index() 
	{	
        $this->view->data['ad']=$this->model->get_admin();
        $this->view->data['cli']=$this->model->get_client();
        $this->view->data['par']=$this->model->get_partner();
        $this->view->data['test']=$this->model->get_testimonial();
        $this->view->data['stu']=$this->model->get_student();
		$this->view->render('gallery/index', $noinclude=false, 3);
	}	
	function add_admin_picture()
	{
		if(isset($_POST['username'])){

			$username=$_POST['username'];
			$pix=$_POST['pix'];
		$destination='c:\xampp\htdocs\tentacular\realestate\public\images\admin'."\\".$_FILES['pix']['username'];
		$temp_file = $_FILES['pix']['tmp_name'];
		move_uploaded_file($temp_file,$destination);
		$pix = $_FILES['pix']['username'];
			$this->model->add_admin_pix($pix,$username);
			Session::set('message_1', "Record Successfully Updated");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
	}
	function add_client_picture()
	{
		if(isset($_POST['name'])){

			$name=$_POST['name'];
			$pix=$_POST['pix'];
		$destination='c:\xampp\htdocs\tentacular\realestate\public\images\clients'."\\".$_FILES['pix']['name'];
		$temp_file = $_FILES['pix']['tmp_name'];
		move_uploaded_file($temp_file,$destination);
		$pix = $_FILES['pix']['name'];
			$this->model->add_client_pix($pix,$name);
			Session::set('message_1', "Record Successfully Updated");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
	}
	function add_partner_picture()
	{
		if(isset($_POST['name'])){

			$name=$_POST['name'];
			$pix=$_POST['pix'];
		$destination='c:\xampp\htdocs\tentacular\realestate\public\images\partners'."\\".$_FILES['pix']['name'];
		$temp_file = $_FILES['pix']['tmp_name'];
		move_uploaded_file($temp_file,$destination);
		$pix = $_FILES['pix']['name'];
			$this->model->add_partner_pix($pix,$name);
			Session::set('message_1', "Record Successfully Updated");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
	}
	function add_testimonial_picture()
	{
		if(isset($_POST['name'])){

			$name=$_POST['name'];
			$pix=$_POST['pix'];
		$destination='c:\xampp\htdocs\tentacular\realestate\public\images\testimonials'."\\".$_FILES['pix']['name'];
		$temp_file = $_FILES['pix']['tmp_name'];
		move_uploaded_file($temp_file,$destination);
		$pix = $_FILES['pix']['name'];
			$this->model->add_testimonial_pix($pix,$name);
			Session::set('message_1', "Record Successfully Updated");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
	}
	function add_student_picture()
	{
		if(isset($_POST['username'])){

			$username=$_POST['username'];
			$pix=$_POST['pix'];
		$destination='c:\xampp\htdocs\tentacular\realestate\public\images\students'."\\".$_FILES['pix']['username'];
		$temp_file = $_FILES['pix']['tmp_name'];
		move_uploaded_file($temp_file,$destination);
		$pix = $_FILES['pix']['name'];
			$this->model->add_student_pix($pix,$name);
			Session::set('message_1', "Record Successfully Updated");
		header("location: ../admin_dashboard");
		}else{
			Session::set('message_1', "Query failed");
		header("location: ../admin_dashboard");
		}
	}
}