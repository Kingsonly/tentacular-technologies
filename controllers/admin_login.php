<?php

class admin_login extends Controller {


	function __construct() {
		parent::__construct();

		
	}
	
	function index() 
	{			
		$this->view->render('admin_login/index', $noinclude=true,0);
	}
    
    
    
    function forgotpwd() 
	{	
		$this->view->render('admin_login/forgotpwd', $noinclude=true,0);
	}
	
 	function login()
 	{
 		if(isset($_POST['username'])){
        $username=$_POST['username'];
		$password=$_POST['password'];
		$this->model->login_user($username,$password);
		header("location: ../admin_dashboard");
		}
		else{
		$this->view->render('admin_login/index', $noinclude=true,0);
		}

 	}
}