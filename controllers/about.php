<?php

class About extends Controller {

	function __construct() {
		parent::__construct();
		
	}
	
	function index() {
        $this->view->data['team']=$this->model->get_team(4);
        $this->view->data['cli']=$this->model->get_clients(5);
        $this->view->data['ser']=$this->model->get_services(6);
        $this->view->render('index/about',$noinclude=false,1);
	}
	
	
	
}