<?php

class Readmail extends Controller {

	function __construct() {
		parent::__construct();
		 Session::init();
		$logged = Session::get('loggedIn');
		$this->view->data['details']=Session::get('details');
		$this->view->data['loggedUser']=Session::get('loggedUser');
		$this->view->data['fullname']=Session::get('fullname');
		$this->view->data['email']=Session::get('email');
		$this->view->data['phone']=Session::get('phone');
		$this->view->data['message_1']=Session::get('message_1');
		$this->view->data['admin_id']=Session::get('admin_id');
		$this->view->data['pix']=Session::get('pix');
		$this->view->data['msg_name']=Session::get('msg_name');
		$this->view->data['msg_id']=Session::get('msg_id');

		$loggedid = Session::get('user_id');
 
		if ($logged == false) {
			Session::destroy();
			//header('location: ./login');
			exit;
		}
	}
	
	function index() 
	{	
			if(isset($_GET['msgr'])){
			$msgr = $_GET['msgr'];
			$this->view->data['msg_body']=$this->model->get_msg($msgr);
		}
		$this->view->render('readmail/index', $noinclude=false, 3);
	}
	function update_status()
	{
		if(isset($_SESSION['msg_id'])){
			$msg_id = $_SESSION['msg_id'];
		$this->model->set_status($msg_id);
		header("location: ../admin_mailbox");
		}
	}
	function delete_msg()
	{
		if(isset($_SESSION['msg_id'])){
			$msg_id = $_SESSION['msg_id'];
		$this->model->delete($msg_id);
		header("location: ../admin_mailbox");
		}
	}

}