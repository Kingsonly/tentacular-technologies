<?php

class Payment2 extends Controller {

	function __construct() {
		parent::__construct();
		
	}
	
	function index() {
        $this->view->render('enroll_student/payment2',$noinclude=false,1);
	}
	
	function register()
	{
		if(isset($_POST['username'])) {
			$username=$_POST['username'];
			$password=$_POST['password'];
			$fullname=$_POST['fullname'];
			$address=$_POST['address'];	
			$email=$_POST['email'];
			$phone=$_POST['phone'];
			$plan=$_POST['plan'];
			$course=$_POST['course'];
			$date = date("Y-m-d");
			$datetime = date("Y-m-d H:i:s");
			if (filter_var($email, FILTER_VALIDATE_EMAIL)) { // this line checks that we have a valid email address
				$check1=$this->model->check_existing_username($username);
				if ($check1>0){
					echo "Username already exists, please select another username";
				}else{
					$check2=$this->model->check_existing_student($fullname,$plan);
 					if($check2>0){
						echo "You have already registered!"; // success message
				}else{
					$this->model->register_student($username,$password,$fullname,$address,$email,$phone,$date,$datetime,$course,$plan);
					//$this->model->add_academy_details($username,$plan,$course);
					echo "You have successfully registered!";
                    header("location:".URL."codeless/payment");
                    
					}}}
			else{
			echo "Invalid Email, please provide an correct email.";
			}
	}
	
}
    
    
    public function payment(){
        
        //$this->view->render('enroll_student/payment',$noinclude=false,1);
        $this->view->render('enroll_student/codeless',$noinclude=false,1);
    }
	
}