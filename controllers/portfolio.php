<?php

class Portfolio extends Controller {

	function __construct() {
		parent::__construct();
		
	}
	
	function index() {
        $this->view->data['port']=$this->model->get_portfolio(12);
		$this->view->data['portac']=$this->model->get_academy_projects(12);
        $this->view->render('index/portfolio',$noinclude=false,1);
	}
	
}