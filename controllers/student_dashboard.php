<?php

class student_dashboard extends Controller {

	function __construct() {
		parent::__construct();
                Session::init();
                $logged = Session::get('loggedIn');
        
                $this->view->data['address']=Session::get('address');
                $this->view->data['username']=Session::get('username');
                $this->view->data['fullname']=Session::get('fullname');
                $this->view->data['email']=Session::get('email');
                $this->view->data['phone']=Session::get('phone');
                $this->view->data['paid_status']=Session::get('paid_status');
                $this->view->data['paid']=Session::get('paid');
                $this->view->data['completed_course']=Session::get('completed_course');
                $this->view->data['academy_remark']=Session::get('academy_remark');
                $this->view->data['student_id']=Session::get('student_id');
                $this->view->data['pix']=Session::get('pix');
                $this->view->data['unread']=Session::get('unread');
                $this->view->data['plan']=Session::get('plan');
                $this->view->data['course']=Session::get('course');
                $this->view->data['classmates']=Session::get('classmates');
 
                if ($logged == false) {
                        header('location: student_login');
                        exit;
                }
	}
	
	function index() {         
            $student_id = Session::get('student_id');
        $this->view->data['mentor']=$this->model->get_mentors();
        $this->view->render('student_dashboard/index',$noinclude=false,5);
	}

        function change_photo()
        {
                if (isset($_POST['student_id'])) {
                       $student_id = $_POST['student_id'];
                        $old_pic = $this->model->get_old_pix($student_id);
                        if ($old_pic !== 'nopix.png') {
                        unlink('public/images/students/'.$old_pic);
                        $pix=($_FILES['photo']['name']);
                        $target = "public/images/students/";
                        $target = $target . basename( $_FILES['photo']['name']);
                        $this->model->change_pix($student_id,$pix);
                        move_uploaded_file($_FILES['photo']['tmp_name'], $target);
        Session::set('pix', $pix);
        header("location: ../student_dashboard");
                        }else{
                        $pix=($_FILES['photo']['name']);
                        $target = "public/images/students/";
                        $target = $target . basename( $_FILES['photo']['name']);
                        $this->model->change_pix($student_id,$pix);
                        move_uploaded_file($_FILES['photo']['tmp_name'], $target);
        Session::set('pix', $pix);
        header("location: ../student_dashboard");
                }}
        }

        function update_student()
        {                            
                if(isset($_POST['update_field'])) {
                        $student_id=$_SESSION['student_id'];
                        $update_field=$_POST['update_field'];
                        $update=$_POST['update'];
                        if ($_POST['update_field'] == 'username'){
                        $check=$this->model->check_existing_username($update);
                        if($check>0){
                        echo "Username already exists";
                        }else{
                        $this->model->update_username($update,$student_id);
                        Session::set('username', $update);
        header("location: ../student_dashboard");
                        }}
                        elseif ($_POST['update_field'] == 'password') {
                        $this->model->update_password($update,$student_id);
                        Session::set('password', $update);
        header("location: ../student_dashboard");
                        }elseif ($_POST['update_field'] == 'fullname') {
                        $this->model->update_fullname($update,$student_id);
                        Session::set('fullname', $update);
        header("location: ../student_dashboard");
                        }
                        elseif ($_POST['update_field'] == 'address') {
                        $this->model->update_address($update,$student_id);
                        Session::set('address', $update);
        header("location: ../student_dashboard");
                        }
                        elseif ($_POST['update_field'] == 'email') {
                        $this->model->update_email($update,$student_id);
                        Session::set('email', $update);
        header("location: ../student_dashboard");
                        }
                        elseif ($_POST['update_field'] == 'phone') {
                        $this->model->update_phone($update,$student_id);
                        Session::set('phone', $update);
        header("location: ../student_dashboard");
                        }}
                }
}