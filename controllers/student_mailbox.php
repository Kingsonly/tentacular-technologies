<?php

class student_mailbox extends Controller {

	function __construct() {
		parent::__construct();
                Session::init();
                $logged = Session::get('loggedIn');
        
                $this->view->data['address']=Session::get('address');
                $this->view->data['username']=Session::get('username');
                $this->view->data['fullname']=Session::get('fullname');
                $this->view->data['email']=Session::get('email');
                $this->view->data['phone']=Session::get('phone');
                $this->view->data['paid_status']=Session::get('paid_status');
                $this->view->data['paid']=Session::get('paid');
                $this->view->data['completed_course']=Session::get('completed_course');
                $this->view->data['academy_remark']=Session::get('academy_remark');
                $this->view->data['student_id']=Session::get('student_id');
                $this->view->data['pix']=Session::get('pix');
                $this->view->data['unread']=Session::get('unread');
                $this->view->data['plan']=Session::get('plan');
                $this->view->data['course']=Session::get('course');
                $this->view->data['classmates']=Session::get('classmates');
 
                if ($logged == false) {
                        header('location: student_login');
                        exit;
                }
	}
	
	function index() {
                if (isset($_SESSION['student_id'])) {
                        $student_id = $_SESSION['student_id'];
                $this->view->data['oldmsg']=$this->model->oldmail($student_id);
                $this->view->data['msg']=$this->model->mailbox($student_id);
                }
        $this->view->render('student_dashboard/mailbox',$noinclude=false,5);
	}

}