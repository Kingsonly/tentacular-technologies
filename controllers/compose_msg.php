<?php

class Compose_msg extends Controller {

	function __construct() {
		parent::__construct();
		Session::init();
                $logged = Session::get('loggedIn');
        
                $this->view->data['address']=Session::get('address');
                $this->view->data['username']=Session::get('username');
                $this->view->data['fullname']=Session::get('fullname');
                $this->view->data['email']=Session::get('email');
                $this->view->data['phone']=Session::get('phone');
                $this->view->data['paid_status']=Session::get('paid_status');
                $this->view->data['paid']=Session::get('paid');
                $this->view->data['completed_course']=Session::get('completed_course');
                $this->view->data['academy_remark']=Session::get('academy_remark');
                $this->view->data['student_id']=Session::get('student_id');
                $this->view->data['pix']=Session::get('pix');
                $this->view->data['unread']=Session::get('unread');
                $this->view->data['plan']=Session::get('plan');
                $this->view->data['course']=Session::get('course');
                $this->view->data['classmates']=Session::get('classmates');
 
                if ($logged == false) {
                        header('location: student_login');
                        exit;
                }
		
	}
	
	function index() {
		if(isset($_GET['student_id'])){
			$student_id = base64_decode(urldecode($_GET['student_id']));
        $this->view->render('student_dashboard/compose',$noinclude=false,5);
	}
	}

	function sendmsg()
	{
		if(isset($_POST['sender'])){
			$sender=$_POST['sender'];
			$student_id=$_POST['student_id'];
			$subject=$_POST['subject'];
			$message=$_POST['message'];	
			$datetime = date("Y-m-d H:i:s");
			$check=$this->model->check_existing_message($message,$student_id);
 		if($check>0){
			echo "You have already sent this message!"; // success message
			}else{
			$this->model->add_msg($sender,$student_id,$subject,$message,$datetime);
                        header('location: ../classmates');
			}
	}
	}
	
	
}