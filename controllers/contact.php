<?php

class Contact extends Controller {

	function __construct() {
		parent::__construct();
		
	}
	
	function index() {
        $this->view->render('index/contact',$noinclude=false,1);
	}
	
	function sendmsg()
	{
		if(isset($_POST['name'])){
			$name=$_POST['name'];
			$email=$_POST['email'];
			$subject=$_POST['subject'];
			$message=$_POST['message'];	
			$date = date("Y-m-d");
			$datetime = date("Y-m-d H:i:s");
			if (filter_var($email, FILTER_VALIDATE_EMAIL)) { // this line checks that we have a valid email address
			$check=$this->model->check_existing_message($message,$email);
 		if($check>0){
			echo "You have already sent this message!"; // success message
			}else{
			$this->model->add_msg($name,$email,$subject,$message,$date,$datetime);
			echo "Your email was sent!"; // success message
			}}
			else{
			echo "Invalid Email, please provide an correct email.";
			}
	}
	}
	
}