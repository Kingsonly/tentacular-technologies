<?php

class student_readmail extends Controller {

	function __construct() {
		parent::__construct();
                Session::init();
                $logged = Session::get('loggedIn');
        
                $this->view->data['address']=Session::get('address');
                $this->view->data['username']=Session::get('username');
                $this->view->data['fullname']=Session::get('fullname');
                $this->view->data['email']=Session::get('email');
                $this->view->data['phone']=Session::get('phone');
                $this->view->data['paid_status']=Session::get('paid_status');
                $this->view->data['paid']=Session::get('paid');
                $this->view->data['completed_course']=Session::get('completed_course');
                $this->view->data['academy_remark']=Session::get('academy_remark');
                $this->view->data['student_id']=Session::get('student_id');
                $this->view->data['pix']=Session::get('pix');
                $this->view->data['unread']=Session::get('unread');
                $this->view->data['plan']=Session::get('plan');
                $this->view->data['course']=Session::get('course');
                $this->view->data['classmates']=Session::get('classmates');
                $this->view->data['msg_id']=Session::get('msg_id'); 
                $this->view->data['sender']=Session::get('sender');
                if ($logged == false) {
                        header('location: student_login');
                        exit;
                }
	}
	
	function index() {
                if(isset($_GET['msg_id'])){
                $msg_id = $_GET['msg_id'];
                $this->view->data['msg_body']=$this->model->get_msg($msg_id);
                }

        $this->view->render('student_dashboard/readmail',$noinclude=false,5);
	}
        function update_status()
        {
                if(isset($_SESSION['msg_id'])){
                        $msg_id = $_SESSION['msg_id'];
                        $fullname = $_SESSION['fullname'];
                $this->model->set_status($msg_id);
                $this->model->update_unread($fullname);
                header('location: ../student_mailbox');
                }
        }
        function reply_msg()
        {
                if(isset($_SESSION['msg_id'])){
                        $msg_id = $_SESSION['msg_id'];
                        $sender = $_SESSION['sender'];
                $this->model->set_status($msg_id);
                $this->model->update_unread($fullname);
                header("location: ../compose_msg?student=$sender");
                }
        }
}