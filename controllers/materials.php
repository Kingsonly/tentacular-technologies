<?php

class materials extends Controller {

	function __construct() {
		parent::__construct();
                Session::init();
        if (isset($_GET['file'])) {
                    // download.php
             if (isset($_GET['file'])) {
            //$file = $_GET['file'];// Always sanitize your submitted data!!!!!!
            //$file = filter_input(INPUT_GET, 'file', FILTER_SANITIZE_ENCODED);// works also
                $file = filter_input(INPUT_GET, 'file', FILTER_SANITIZE_SPECIAL_CHARS);
                    if (file_exists($file) && is_readable($file) && preg_match('/\.pdf$/',$file)) {
                        header('Content-Description: File Transfer');
                        header('Content-type: application/pdf');
                        header("Content-Type: application/force-download");// some browsers need this
                        header("Content-Disposition: attachment; filename=\"$file\"");
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                        header('Pragma: public');
                        header('Content-Length: ' . filesize($file));
                        ob_clean();
                        flush();
                        readfile($file);
                            exit;
                    }else {
                        header("HTTP/1.0 404 Not Found");
                        echo "<h3>Error 404: File Not Found: <br /><em>$file</em></h3>";
                        header('Refresh: 5; url=./index.html');
                        print '<i style="color:red">You will be redirected in 5 seconds</i>';
                        exit ;
                    }
            }else {
                header('Refresh: 5; url=./index.html');
                print '<h1 style="text-align:center">You you shouldn\'t be here ......</h1><br> <p style="color:red"><b>redirection in 5 seconds</b></p>';
                exit;
            }
        }
 
                
                $logged = Session::get('loggedIn');
        
                $this->view->data['address']=Session::get('address');
                $this->view->data['username']=Session::get('username');
                $this->view->data['fullname']=Session::get('fullname');
                $this->view->data['email']=Session::get('email');
                $this->view->data['phone']=Session::get('phone');
                $this->view->data['paid_status']=Session::get('paid_status');
                $this->view->data['paid']=Session::get('paid');
                $this->view->data['completed_course']=Session::get('completed_course');
                $this->view->data['academy_remark']=Session::get('academy_remark');
                $this->view->data['student_id']=Session::get('student_id');
                $this->view->data['pix']=Session::get('pix');
                $this->view->data['unread']=Session::get('unread');
                $this->view->data['plan']=Session::get('plan');
                $this->view->data['course']=Session::get('course');
                $this->view->data['classmates']=Session::get('classmates');
 
                if ($logged == false) {
                        header('location: student_login');
                        exit;
                }
	}
	
	function index() {
                
                        $id=Session::get('student_id');
                        $type="pdf";
                        $type2="video";
                    
                        $this->view->data['material']=$this->model->get_materials($id,$type); 
                        $this->view->data['material2']=$this->model->get_materials($id,$type2); 
                  $this->view->render('student_dashboard/materials',$noinclude=false,5);
	}

}