<?php

class admin_mailbox extends Controller {

	function __construct() {
		parent::__construct();
		 Session::init();
		$logged = Session::get('loggedIn');
		$this->view->data['details']=Session::get('details');
		$this->view->data['loggedUser']=Session::get('loggedUser');
		$this->view->data['fullname']=Session::get('fullname');
		$this->view->data['email']=Session::get('email');
		$this->view->data['phone']=Session::get('phone');
		$this->view->data['message_1']=Session::get('message_1');
		$this->view->data['admin_id']=Session::get('admin_id');
		$this->view->data['pix']=Session::get('pix');
		$this->view->data['unread']=Session::get('unread');
		$this->view->data['msg_id']=Session::get('msg_id');

		$loggedid = Session::get('user_id');
 
		if ($logged == false) {
			Session::destroy();
			//header('location: ./login');
			exit;
		}
	}
	
	function index() 
	{	
		$this->view->data['msg']=$this->model->mailbox();
		$this->view->render('admin_mailbox/index', $noinclude=false, 3);
	}
}