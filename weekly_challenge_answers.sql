-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2017 at 01:12 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tent`
--

-- --------------------------------------------------------

--
-- Table structure for table `weekly_challenge_answers`
--

CREATE TABLE IF NOT EXISTS `weekly_challenge_answers` (
  `answer_id` int(255) NOT NULL AUTO_INCREMENT,
  `challenge_id` int(255) NOT NULL,
  `challenge_name` varchar(5000) NOT NULL,
  `challenge_date` date NOT NULL,
  `answer` varchar(5000) NOT NULL,
  `answer_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `student_name` varchar(100) NOT NULL,
  `ratings` int(255) NOT NULL DEFAULT '0',
  `admin_ratings` int(10) NOT NULL DEFAULT '0',
  `status` varchar(10) NOT NULL DEFAULT 'NO',
  PRIMARY KEY (`answer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `weekly_challenge_answers`
--

INSERT INTO `weekly_challenge_answers` (`answer_id`, `challenge_id`, `challenge_name`, `challenge_date`, `answer`, `answer_date`, `student_name`, `ratings`, `admin_ratings`, `status`) VALUES
(1, 1, 'Challenge01', '2017-05-11', 'ANSWER', '2017-05-12 19:02:37', 'Dahlia Akhaine', 0, 0, 'ON'),
(2, 1, 'Challenge01', '2017-05-11', 'boris answer', '2017-05-13 04:54:17', 'Bori Matthew', 0, 0, 'ON');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
