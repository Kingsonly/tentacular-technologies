<?php

class gallery_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
		
	}
	public function get_admin()
	{
		$sth = $this->db->query("SELECT * FROM  admin ORDER BY  username DESC");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function add_admin_pix($pix,$username)
	{
		$sql = "UPDATE admin SET pix = '$pix' WHERE username = '$username'";
		$this->db->query($sql);
	}
	public function get_client()
	{
		$sth = $this->db->query("SELECT * FROM  clients ORDER BY  name DESC");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function add_client_pix($pix,$name)
	{
		$sql = "UPDATE clients SET pix = '$pix' WHERE name = '$name'";
		$this->db->query($sql);
	}
	public function get_partner()
	{
		$sth = $this->db->query("SELECT * FROM  partners ORDER BY  name DESC");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function add_partner_pix($pix,$name)
	{
		$sql = "UPDATE partners SET pix = '$pix' WHERE name = '$name'";
		$this->db->query($sql);
	}
	public function get_testimonial()
	{
		$sth = $this->db->query("SELECT * FROM  testimonials ORDER BY  name DESC");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function add_testimonial_pix($pix,$name)
	{
		$sql = "UPDATE testimonials SET pix = '$pix' WHERE name = '$name'";
		$this->db->query($sql);
	}
	public function get_student()
	{
		$sth = $this->db->query("SELECT * FROM  academy_students ORDER BY  username DESC");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function add_student_pix($pix,$username)
	{
		$sql = "UPDATE academy_students SET pix = '$pix' WHERE username = '$username'";
		$this->db->query($sql);
	}
}
?>