<?php

class records_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
		
	}
	public function get_admin()
	{
		$sth = $this->db->query("SELECT * FROM  admin ORDER BY  username DESC");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function check_existing_admin($username)
	{
		$sth = $this->db->query("SELECT * FROM admin WHERE username='$username'");
            $rowss=$sth->num_rows;
            return $rowss;
	}
	public function add_admin($username,$password,$fullname,$details,$email,$phone)
	{
		$sql = "INSERT INTO admin (username,password,fullname,details,email,phone)
			VALUES ('$username','$password','$fullname','$details','$email','$phone')";

		 $this->db->query($sql);
	}
	public function update_admin($update_field,$update,$username)
	{
		$sql = "UPDATE admin SET $update_field = '$update' WHERE username = '$username'";
		$this->db->query($sql);
	}
	public function delete_admin($username)
	{
		$sql = "DELETE FROM admin WHERE username = '$username'";
		$this->db->query($sql);
	}
	public function get_student_names()
	{
		$sth = $this->db->query("SELECT * FROM  academy_students ORDER BY  username DESC");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function check_existing_student($username)
	{
		$sth = $this->db->query("SELECT * FROM academy_students WHERE username='$username'");
            $rowss=$sth->num_rows;
            return $rowss;
	}
	public function add_student($username,$password,$fullname,$plan,$address,$email,$phone,$paid_status,$paid,$completed_course,$academy_remark)
	{
		$sql = "INSERT INTO academy_students (username,password,fullname,plan,address,email,phone,paid_status,paid,completed_course,academy_remark,pix)
			VALUES ('$username','$password','$fullname','$plan','$address','$email','$phone','$paid_status','$paid','$completed_course','$academy_remark')";

		 $this->db->query($sql);
	}
	public function update_student($update_field,$update,$username)
	{
		$sql = "UPDATE academy_students SET $update_field = '$update' WHERE username = '$username'";
		$this->db->query($sql);
	}
	public function delete_student($username)
	{
		$sql = "DELETE FROM academy_students WHERE username = '$username'";
		$this->db->query($sql);
	}
	public function get_clients()
	{
		$sth = $this->db->query("SELECT * FROM  clients ORDER BY  name DESC");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function check_existing_client($name)
	{
		$sth = $this->db->query("SELECT * FROM clients WHERE name='$name'");
            $rowss=$sth->num_rows;
            return $rowss;
	}
	public function add_client($name,$details,$pix,$contact)
	{
		$sql = "INSERT INTO clients (name,details,pix,contact)
			VALUES ('$name','details','$pix','$contact')";

		 $this->db->query($sql);
	}
	public function update_clients($update_field,$update,$name)
	{
		$sql = "UPDATE clients SET $update_field = '$update' WHERE name = '$name'";
		$this->db->query($sql);
	}
	public function delete_client($name)
	{
		$sql = "DELETE FROM clients WHERE name = '$name'";
		$this->db->query($sql);
	}
	public function get_partner()
	{
		$sth = $this->db->query("SELECT * FROM  partners ORDER BY  name DESC");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function check_existing_partner($name)
	{
		$sth = $this->db->query("SELECT * FROM partners WHERE name='$name'");
            $rowss=$sth->num_rows;
            return $rowss;
	}
	public function add_partner($name)
	{
		$sql = "INSERT INTO partners (name)
			VALUES ('$name')";

		 $this->db->query($sql);
	}
	public function delete_partner($name)
	{
		$sql = "DELETE FROM partners WHERE name = '$name'";
		$this->db->query($sql);
	}
	public function get_testimonials()
	{
		$sth = $this->db->query("SELECT * FROM  testimonials ORDER BY  name DESC");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function check_existing_testimonial($name)
	{
		$sth = $this->db->query("SELECT * FROM testimonials WHERE name='$name'");
            $rowss=$sth->num_rows;
            return $rowss;
	}
	public function add_testimonial($name,$address,$comment)
	{
		$sql = "INSERT INTO testimonials (name,address,comment)
			VALUES ('$name','$address','$comment')";

		 $this->db->query($sql);
	}
	public function delete_testimonial($name)
	{
		$sql = "DELETE FROM testimonials WHERE name = '$name'";
		$this->db->query($sql);
	}
	public function get_service()
	{
		$sth = $this->db->query("SELECT * FROM  services ORDER BY  title DESC");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function check_existing_service($title)
	{
		$sth = $this->db->query("SELECT * FROM services WHERE title='$title'");
            $rowss=$sth->num_rows;
            return $rowss;
	}
	public function add_service($title,$service)
	{
		$sql = "INSERT INTO services (title,service)
			VALUES ('$title','$service')";

		 $this->db->query($sql);
	}
	public function delete_service($title)
	{
		$sql = "DELETE FROM services WHERE title = '$title'";
		$this->db->query($sql);
	}public function get_terms()
	{
		$sth = $this->db->query("SELECT * FROM  terms ORDER BY  term DESC");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function check_existing_term($term)
	{
		$sth = $this->db->query("SELECT * FROM terms WHERE term='$term'");
            $rowss=$sth->num_rows;
            return $rowss;
	}
	public function add_term($term,$details)
	{
		$sql = "INSERT INTO terms (term,details)
			VALUES ('$term','$details')";

		 $this->db->query($sql);
	}
	public function delete_term($term)
	{
		$sql = "DELETE FROM terms WHERE term = '$term'";
		$this->db->query($sql);
	}
}
?>