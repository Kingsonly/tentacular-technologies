<?php

class portfolio_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
		
	}
	public function get_portfolio($limit)
	{
		$sth = $this->db->query("SELECT * FROM portfolio ORDER BY portfolio_id ASC LIMIT $limit");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function get_academy_projects($limit)
	{
		$sth = $this->db->query("SELECT * FROM academy_projects ORDER BY project_id DESC limit $limit");
		$out_put=$sth->rows;
        
		return $out_put;
	}
}
?>