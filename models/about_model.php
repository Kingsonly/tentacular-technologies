<?php

class about_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
		
	}
	public function get_team($limit)
	{
		$sth = $this->db->query("SELECT * FROM team ORDER BY team_id ASC LIMIT $limit");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function get_clients($limit)
	{
		$sth = $this->db->query("SELECT * FROM portfolio ORDER BY portfolio_id DESC LIMIT $limit");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function get_services($limit)
	{
		$sth = $this->db->query("SELECT * FROM services ORDER BY title DESC LIMIT $limit");
		$out_put=$sth->rows;
        
		return $out_put;
	}
}
?>