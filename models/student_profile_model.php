<?php
class Student_profile_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function get_classmate_profile($student_id)
	{
		$sth = $this->db->query("SELECT * FROM academy_students WHERE student_id = '$student_id'");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function get_classmate_academy_details($student_id)
	{
		$sth = $this->db->query("SELECT * FROM student_academy_details WHERE student_id = '$student_id'");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function get_answers_timeline($student_id)
	{
		$sth = $this->db->query("SELECT * FROM weekly_challenge_answers WHERE student_id = '$student_id'");
		$out_put=$sth->rows;
        
		return $out_put;
	}
}
?>