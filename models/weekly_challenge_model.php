<?php
class Weekly_challenge_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function get_weekly_challenge_answers()
	{
		$sth = $this->db->query("SELECT * FROM weekly_challenge_answers WHERE status = 'ON' ORDER BY answer_id DESC");
		$out_put=$sth->rows;
        
		return $out_put;
	}

	public function get_total_weekly_challenge_answers()
	{
		$sth = $this->db->query("SELECT * FROM weekly_challenge_answers WHERE status = 'ON' ORDER BY answer_id DESC");
		$out_put=$sth->num_rows;
        
		return $out_put;
	}

	public function get_weekly_challenge_details()
	{
		$sth = $this->db->query("SELECT * FROM weekly_challenge WHERE status = 'ON'");
		$out_put=$sth->rows;
        
		return $out_put;
	}

	public function check_existing_answer($student_name,$answer)
	{
		$sth = $this->db->query("SELECT * FROM weekly_challenge_answers WHERE student_name ='$student_name' AND answer='$answer'");
            $rowss=$sth->num_rows;
            return $rowss;
	}

	public function add_answer($student_name,$student_id,$answer,$challenge_id,$challenge_name,$challenge_date,$status)
	{
		$sql = "INSERT INTO weekly_challenge_answers (student_name,student_id,answer,challenge_id,challenge_name,challenge_date,status)
			VALUES ('$student_name','$student_id','$answer','$challenge_id','$challenge_name','$challenge_date','$status')";

		 $this->db->query($sql);
	}
}
?>