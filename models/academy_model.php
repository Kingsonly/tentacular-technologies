<?php

class academy_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
		
	}
	public function get_academy_projects($limit)
	{
		$sth = $this->db->query("SELECT * FROM academy_projects ORDER BY project_id DESC limit $limit");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function get_pricing_plans($limit)
	{
		$sth = $this->db->query("SELECT * FROM academy_pricing_plans ORDER BY plan_id ASC LIMIT $limit");
		$out_put=$sth->rows;
        
		return $out_put;
	}
}
?>