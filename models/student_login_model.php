<?php
class student_login_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function unread($student_id)
	{
		$sql = $this->db->query("SELECT * FROM  student_msg WHERE (student_id = '$student_id' AND status = 'unread') OR (student_id = 0 AND status = 'unread')");
		$unread=$sql->num_rows;
			Session::set('unread', $unread);
	}
	public function login_student($username,$password)
	{
		$sth = $this->db->query("SELECT * FROM  academy_students WHERE username ='$username' AND password = '$password'");
		$count =  $sth->num_rows;
		if ($count > 0) {
            $student_id =  $sth->row['student_id'];
		    $username=  $sth->row['username'];
		    $fullname = $sth->row['fullname'];
		    $address = $sth->row['address'];
			$email=	$sth->row['email'];
			$phone = $sth->row['phone'];
		    $paid_status = $sth->row['paid_status'];
			$paid=	$sth->row['paid'];
			$pix = $sth->row['pix'];

			Session::set('loggedIn', true);
			Session::set('username', $username);
			Session::set('student_id', $student_id);
			Session::set('fullname', $fullname);
			Session::set('address', $address);
			Session::set('email', $email);
			Session::set('phone', $phone);
			Session::set('paid_status', $paid_status);
			Session::set('paid', $paid);
			Session::set('pix', $pix);
           // echo '<script> location.replace("../dashboard"); </script>';
		}	
	}

	public function get_academy_details($username)
	{
		$sth = $this->db->query("SELECT * FROM  student_academy_details WHERE username ='$username'");
		$count =  $sth->num_rows;
		if ($count > 0) {
            $plan =  $sth->row['plan'];
		    $course=  $sth->row['course'];

			Session::init();
			Session::set('plan', $plan);
			Session::set('course', $course);
		}	
	}
	
	public function classmates($course,$student_id)
	{
		$sth = $this->db->query("SELECT * FROM academy_students WHERE course = '$course' AND student_id != '$student_id'");
            $classmates=$sth->num_rows;
            Session::set('classmates', $classmates);
	}
}
?>