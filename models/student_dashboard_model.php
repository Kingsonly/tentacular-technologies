<?php

class student_dashboard_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
		
	}
	public function get_old_pix($student_id)
	{		
		$sth = $this->db->query("SELECT * FROM academy_students WHERE student_id = '$student_id'");
		$count =  $sth->num_rows;
		if ($count > 0) {
            $pix =  $sth->row['pix'];
            return $pix;
        }
	}   
	public function change_pix($student_id,$pix)
	{
		$sql = "UPDATE academy_students SET pix = '$pix' WHERE student_id = '$student_id'";
		$this->db->query($sql);
	}
	public function check_existing_username($update)
	{
		$sth = $this->db->query("SELECT * FROM academy_students WHERE username='$update'");
            $rowss=$sth->num_rows;
            return $rowss;
	}
	public function get_mentors()
	{
		$sth = $this->db->query("SELECT fullname FROM admin ORDER BY admin_id DESC");
            $rowss=$sth->rows;
            return $rowss;
	}
	public function update_username($update,$student_id)
	{
		$sql = "UPDATE academy_students SET username = '$update' WHERE student_id = '$student_id'";
		$this->db->query($sql);
	}
	public function update_password($update,$student_id)
	{
		$sql = "UPDATE academy_students SET password = '$update' WHERE student_id = '$student_id'";
		$this->db->query($sql);
	}
	public function update_fullname($update,$student_id)
	{
		$sql = "UPDATE academy_students SET fullname = '$update' WHERE student_id = '$student_id'";
		$this->db->query($sql);
	}
	public function update_address($update,$student_id)
	{
		$sql = "UPDATE academy_students SET address = '$update' WHERE student_id = '$student_id'";
		$this->db->query($sql);
	}
	public function update_email($update,$student_id)
	{
		$sql = "UPDATE academy_students SET email= '$update' WHERE student_id = '$student_id'";
		$this->db->query($sql);
	}
	public function update_phone($update,$student_id)
	{
		$sql = "UPDATE academy_students SET phone= '$update' WHERE student_id = '$student_id'";
		$this->db->query($sql);
	}

}
?>