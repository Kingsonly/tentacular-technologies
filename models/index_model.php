<?php

class index_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
		
	}
	public function check_existing_email($email)
	{
		$sth = $this->db->query("SELECT * FROM mailing_list WHERE email='$email'");
            $rowss=$sth->num_rows;
            return $rowss;
	}
	public function register_email($email)
	{
		$sql = "INSERT INTO mailing_list (email)
			VALUES ('$email')";

		 $this->db->query($sql);
	}
	public function get_services($limit)
	{
		$sth = $this->db->query("SELECT * FROM services ORDER BY title ASC LIMIT $limit");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function get_team($limit)
	{
		$sth = $this->db->query("SELECT * FROM team ORDER BY team_id ASC LIMIT $limit");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function get_portfolio($limit)
	{
		$sth = $this->db->query("SELECT * FROM portfolio ORDER BY portfolio_id ASC LIMIT $limit");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function get_pricing_plans($limit)
	{
		$sth = $this->db->query("SELECT * FROM academy_pricing_plans ORDER BY plan_id ASC");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function get_partners($limit)
	{
		$sth = $this->db->query("SELECT * FROM partners ORDER BY partner_id ASC LIMIT $limit");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function get_news($limit)
	{
		$sth = $this->db->query("SELECT * FROM newsfeed ORDER BY news_id DESC LIMIT $limit");
		$out_put=$sth->rows;
        
		return $out_put;
	}
	public function get_testimonials($limit)
	{
		$sth = $this->db->query("SELECT * FROM testimonials ORDER BY testimony_id DESC LIMIT $limit");
		$out_put=$sth->rows;
        
		return $out_put;
	}
}
?>