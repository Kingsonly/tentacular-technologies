<?php
class Admin_readmail_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function get_msg($msgr)
	{
		$sth = $this->db->query("SELECT * FROM admin_msg WHERE msg_id = '$msgr'");
        $msg_name = $sth->row['name'];
		Session::set('msg_name', $msg_name);
		$msg_id = $sth->row['msg_id'];
		Session::set('msg_id', $msg_id);
		$out_put=$sth->rows;
		return $out_put;
	}
	public function set_status($msg_id)
	{
		$sql = "UPDATE admin_msg SET status = 'read' WHERE msg_id = '$msg_id'";
		$this->db->query($sql);
	}
	public function delete($msg_id)
	{
		$sql = "DELETE FROM admin_msg WHERE msg_id = '$msg_id'";
		$this->db->query($sql);
	}
}
?>