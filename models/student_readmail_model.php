<?php
class Student_readmail_Model extends Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function get_msg($msg_id)
	{
		$sth = $this->db->query("SELECT * FROM student_msg WHERE msg_id = '$msg_id'");
		$msg_id = $sth->row['msg_id'];
		Session::set('msg_id', $msg_id);
		$sender = $sth->row['sender'];
		Session::set('sender', $sender);
		$out_put=$sth->rows;
		return $out_put;
	}
	public function set_status($msg_id)
	{
		$sql = "UPDATE student_msg SET status = 'read' WHERE msg_id = '$msg_id'";
		$this->db->query($sql);
	}
	public function update_unread($fullname)
	{
		$sql = $this->db->query("SELECT * FROM  student_msg WHERE (receiver = '$fullname' AND status = 'unread') OR (receiver = 'all' AND status = 'unread')");
		$unread=$sql->num_rows;
			Session::set('unread', $unread);
	}
}
?>